/*     */ package com.hbolag.bncauthcenter;
/*     */ import java.sql.Timestamp;
/*     */ import java.util.Collection;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/*     */ 
/*     */ import com.hbolag.bncauthcenter.dao.AuthCenterDAO;
/*     */ import com.hbolag.bncauthcenter.structures.UnitData;
/*     */ import com.hbolag.bncauthcenter.utilities.BNCAuthCenterHelper;
/*     */ import com.hbolag.bncauthcenter.utilities.BNCExternalClientAPIHelper;
/*     */ import com.hbolag.bncauthcenter.utilities.DateTimeHelper;
/*     */ import com.motorola.bnc.common.shared.structures.ACP;
/*     */ import com.motorola.bnc.common.shared.structures.Affiliate;
import com.motorola.bnc.common.shared.structures.Channel;
/*     */ import com.motorola.bnc.common.shared.structures.DecoderModel;
import com.motorola.bnc.common.shared.structures.DecoderModelNumber;
import com.motorola.bnc.common.shared.structures.DecoderModelType;
import com.motorola.bnc.common.shared.structures.DecoderPreset;
/*     */ import com.motorola.bnc.common.shared.structures.IRD;
/*     */ import com.motorola.bnc.common.shared.structures.Location;
/*     */ import com.motorola.bnc.common.shared.structures.MAD;
/*     */ import com.motorola.bnc.common.shared.structures.RatingRegion;
/*     */ import com.motorola.bnc.common.shared.structures.VersionInfo;
/*     */ import com.motorola.bnc.proxy.external.IExternalClientAPI;
/*     */ import com.motorola.refarch.infrastruct.errdiag.MotStatusException;
/*     */ import com.motorola.refarch.infrastruct.gendata.BaseOperatorGroup;
/*     */ import com.motorola.refarch.infrastruct.gendata.BasePartition;
/*     */ import com.motorola.refarch.infrastruct.gendata.BaseRegion;
/*     */ import com.motorola.refarch.shared.auth.DCIIUnitFilter;
/*     */ 
/*     */ public class BNCAuthCenter
/*     */   implements IBNCAuthCenterAPI
/*     */ {
/*  34 */   private static final Logger logger = LogManager.getLogger(BNCAuthCenter.class);


/*  35 */   private IExternalClientAPI bncExternalClientAPI = null;
/*     */ 
/*     */   public BNCAuthCenter(IExternalClientAPI api)
/*     */   {
/*  40 */     this.bncExternalClientAPI = api;
/*     */   }
/*     */ 
/*     */   public String getBNCVersion()
/*     */     throws MotStatusException
/*     */   {
/*  46 */     if (this.bncExternalClientAPI == null) {
/*  47 */       return null;
/*     */     }
/*  49 */     VersionInfo verInfo = this.bncExternalClientAPI.queryVersion();
/*     */ 
/*  51 */     return verInfo.getServerVersion();
/*     */   }
/*     */ 
/*     */   public void setIRD(UnitData unitData) throws MotStatusException
/*     */   {
/*  56 */     BasePartition partition = null;
/*  57 */     Affiliate affiliate = null;
/*  58 */     Location location = null;
/*  59 */     BaseOperatorGroup OperatorGroup = null;
/*  60 */     BaseRegion region = null;
/*  61 */     RatingRegion ratingRegion = null;
/*  62 */     DecoderModel decoderModel = null;
/*  63 */     List<Channel> virtualChannels = null;
/*  64 */     IRD newIRD = IRD.getBlank();
/*  65 */     boolean forceRegen = false;
/*     */ 
/*  67 */     if (this.bncExternalClientAPI == null) {
/*  68 */       return;
/*     */     }
 
		     if (this.bncExternalClientAPI.irdExists(new DCIIUnitFilter(unitData.getUnitAddress())))
		     {		    	
		    	 newIRD = this.bncExternalClientAPI.queryIRD(new DCIIUnitFilter(unitData.getUnitAddress()));		
		    	 if (newIRD.getModel().getKey().matches("N/A")) {
		    		 decoderModel = BNCExternalClientAPIHelper.getDefaultDecoderModel(this.bncExternalClientAPI, unitData.getDecoderModel(), unitData.getDecoderModelType());		 
		    		 newIRD.setModel(decoderModel);
		    	 }
		 
		    	 if (BNCAuthCenterHelper.isDecoderModelNumberDSR_640N(newIRD.getModel()))
		    	 {
		    		 virtualChannels = BNCExternalClientAPIHelper.setVirtualChannelsDSR_640N(unitData.getSingleVCMID(), unitData.getTierNumbersExt(), unitData.getIsAuthorizedExtTiers(), this.bncExternalClientAPI);		 
		    	 }
		 
		    	 if (BNCAuthCenterHelper.isDecoderModelNumberDSR_6050(newIRD.getModel())) {
		    		 DecoderPreset decoderPreset = bncExternalClientAPI.queryModelDefaultDecoderPreset(1, newIRD.getModel().getModelType(), newIRD.getModel().getModelNumber());//first input is partition number
		    		 newIRD.setLastAppliedPreset(decoderPreset.getThePresetID().getPresetNumber());	
		    	 }
		    	 else {
		    		 newIRD.setLastAppliedPreset(1);
		    	 }

		    	 newIRD = BNCAuthCenterHelper.populateIRD(unitData, newIRD, virtualChannels);
		     }
		     else
		     {              	
		    	 forceRegen = true;
 
		    	 partition = BNCExternalClientAPIHelper.getDefaultPartition(this.bncExternalClientAPI);
		    	 affiliate = BNCExternalClientAPIHelper.getDefaultAffiliate(this.bncExternalClientAPI, partition);
		    	 ratingRegion = BNCExternalClientAPIHelper.getDefaultRatingRegion(this.bncExternalClientAPI);
		    	 location = BNCExternalClientAPIHelper.getDefaultLocation(this.bncExternalClientAPI, affiliate, partition, ratingRegion);
		    	 OperatorGroup = BNCExternalClientAPIHelper.getDefaultOperatorGroup(this.bncExternalClientAPI);
		    	 region = BNCExternalClientAPIHelper.getDefaultRegion(this.bncExternalClientAPI, OperatorGroup.getDBUID());

				decoderModel = BNCExternalClientAPIHelper.getDefaultDecoderModel(this.bncExternalClientAPI, unitData.getDecoderModel(), unitData.getDecoderModelType());
 
				if (BNCAuthCenterHelper.isDecoderModelNumberDSR_640N(decoderModel))
				{
					virtualChannels = BNCExternalClientAPIHelper.setVirtualChannelsDSR_640N(unitData.getSingleVCMID(), unitData.getTierNumbersExt(), unitData.getIsAuthorizedExtTiers(), this.bncExternalClientAPI);
				}

		    	 if (decoderModel.isModelNumber(DecoderModelNumber.DSR_6050)) {
		    		 DecoderPreset decoderPreset = bncExternalClientAPI.queryModelDefaultDecoderPreset(1, decoderModel.getModelType(), decoderModel.getModelNumber());//first input is partition number
		    		 newIRD.setLastAppliedPreset(decoderPreset.getThePresetID().getPresetNumber());	
		    	 }
		    	 else {
		    		 newIRD.setLastAppliedPreset(1);
		    	 }

				
				newIRD = BNCAuthCenterHelper.populateIRD(unitData, partition, affiliate, location, OperatorGroup, region, ratingRegion, decoderModel, virtualChannels);
		     }
		     
		  
		     
		     //newIRD.setLastAppliedPreset(1);
		     this.bncExternalClientAPI.storeIRD(newIRD, forceRegen);
	}


/*     */ 
/*     */   public void tripIRD(String irdUnitAddress)
/*     */     throws MotStatusException
/*     */   {
/* 145 */     if (this.bncExternalClientAPI == null) {
/* 146 */       return;
/*     */     }
/* 148 */     logger.info("tripIRD for UnitAddress: " + irdUnitAddress);
/* 149 */     DCIIUnitFilter unitFilter = new DCIIUnitFilter(irdUnitAddress);
/*     */ 
/* 151 */     if (this.bncExternalClientAPI.irdExists(unitFilter))
/* 152 */       this.bncExternalClientAPI.tripIRD(unitFilter);
/*     */   }
/*     */ 
/*     */   public void deleteIRD(String irdUnitAddress)
/*     */     throws MotStatusException
/*     */   {
/* 191 */     if (this.bncExternalClientAPI == null) {
/* 192 */       return;
/*     */     }
/* 194 */     logger.info("deleteIRD for UnitAddress: " + irdUnitAddress);
/* 195 */     DCIIUnitFilter unitFilter = new DCIIUnitFilter(irdUnitAddress);
/*     */ 
/* 197 */     if (this.bncExternalClientAPI.irdExists(unitFilter))
/* 198 */       this.bncExternalClientAPI.deleteIRD(unitFilter);
/*     */   }
/*     */ 
/*     */   public void setACP(UnitData unitData)
/*     */     throws MotStatusException
/*     */   {
/* 204 */     BasePartition partition = null;
/* 205 */     Affiliate affiliate = null;
/* 206 */     Location location = null;
/* 207 */     BaseOperatorGroup OperatorGroup = null;
/* 208 */     BaseRegion region = null;
/* 209 */     RatingRegion ratingRegion = null;
/* 210 */     DecoderModel decoderModel = null;
/* 211 */     MAD newMad = MAD.getBlank();
/* 212 */     ACP newACP = ACP.getBlank();
/* 213 */     boolean forceRegen = false;
/*     */ 
/* 215 */     if (this.bncExternalClientAPI == null) {
/* 216 */       return;
/*     */     }
/* 218 */     logger.info("setACP for ACP Anchor: " + unitData.getAnchorUnitAddress() + " and ACP Unit Address: " + unitData.getUnitAddress());
/*     */ 
/* 221 */     if (this.bncExternalClientAPI.acpExists(new DCIIUnitFilter(unitData.getUnitAddress())))
/*     */     {
/* 223 */       newACP = this.bncExternalClientAPI.queryACP(new DCIIUnitFilter(unitData.getUnitAddress()));
/* 224 */       newACP = BNCAuthCenterHelper.populateACP(unitData, newACP);
/*     */     }
/*     */     else {
/* 227 */       forceRegen = true;
/*     */ 
/* 229 */       partition = BNCExternalClientAPIHelper.getDefaultPartition(this.bncExternalClientAPI);
/*     */ 
/* 232 */       affiliate = BNCExternalClientAPIHelper.getDefaultAffiliate(this.bncExternalClientAPI, partition);
/*     */ 
/* 235 */       ratingRegion = BNCExternalClientAPIHelper.getDefaultRatingRegion(this.bncExternalClientAPI);
/*     */ 
/* 238 */       location = BNCExternalClientAPIHelper.getDefaultLocation(this.bncExternalClientAPI, affiliate, partition, ratingRegion);
/*     */ 
/* 241 */       OperatorGroup = BNCExternalClientAPIHelper.getDefaultOperatorGroup(this.bncExternalClientAPI);
/*     */ 
/* 244 */       region = BNCExternalClientAPIHelper.getDefaultRegion(this.bncExternalClientAPI, OperatorGroup.getDBUID());
/*     */ 
/* 247 */       decoderModel = BNCExternalClientAPIHelper.getDefaultDecoderModel(this.bncExternalClientAPI, unitData.getDecoderModel(), unitData.getDecoderModelType());
/*     */ 
/* 250 */       newMad = BNCExternalClientAPIHelper.getMADUnitAddress(this.bncExternalClientAPI, unitData.getAnchorUnitAddress());
/* 251 */       if (newMad == null)
/*     */       {
/* 253 */         newMad = BNCAuthCenterHelper.populateMAD(unitData, partition, location, OperatorGroup, decoderModel);
/* 254 */         newMad = BNCExternalClientAPIHelper.storeMADUnitAddress(this.bncExternalClientAPI, newMad);
/*     */       }
/*     */ 
/* 257 */       newACP = BNCAuthCenterHelper.populateACP(unitData, partition, newMad, OperatorGroup, region);
/*     */     }
/*     */ 
/* 260 */     this.bncExternalClientAPI.storeACP(newACP, forceRegen);
/*     */   }
/*     */ 
/*     */   public void tripACP(String acpUnitAddress) throws MotStatusException
/*     */   {
/* 265 */     if (this.bncExternalClientAPI == null) {
/* 266 */       return;
/*     */     }
/* 268 */     logger.info("tripACP for ACP Unit Address: " + acpUnitAddress);
/* 269 */     DCIIUnitFilter unitFilter = new DCIIUnitFilter(acpUnitAddress);
/*     */ 
/* 271 */     if (this.bncExternalClientAPI.acpExists(unitFilter))
/* 272 */       this.bncExternalClientAPI.tripACP(unitFilter);
/*     */   }
/*     */ 
/*     */   public void deleteACP(String anchorUnitAddress, String acpUnitAddress)
/*     */     throws MotStatusException
/*     */   {
/* 311 */     if (this.bncExternalClientAPI == null) {
/* 312 */       return;
/*     */     }
/* 314 */     logger.info("deleteACP for Anchor UnitAddress: " + anchorUnitAddress + " ACP Unit Address: " + acpUnitAddress);
/* 315 */     DCIIUnitFilter unitFilter = new DCIIUnitFilter(acpUnitAddress);
/*     */ 
/* 317 */     if (this.bncExternalClientAPI.acpExists(unitFilter))
/*     */     {
/* 319 */       this.bncExternalClientAPI.deleteACP(unitFilter);
/*     */ 
/* 321 */       if (BNCExternalClientAPIHelper.isMADEmpty(this.bncExternalClientAPI, anchorUnitAddress))
/* 322 */         this.bncExternalClientAPI.deleteMAD(new DCIIUnitFilter(anchorUnitAddress));
/*     */     }
/*     */   }
/*     */ 
/*     */   public void queryAllIRDUnitAddresses(String serverIPAddres) throws MotStatusException
/*     */   {
/* 328 */     BasePartition partition = null;
/* 329 */     Affiliate affiliate = null;
/* 330 */     Location location = null;
/* 331 */     RatingRegion ratingRegion = null;
/* 332 */     AuthCenterDAO authCenterDAO = new AuthCenterDAO();
/*     */ 
/* 334 */     if (this.bncExternalClientAPI == null) {
/* 335 */       return;
/*     */     }
/* 337 */     Timestamp processedTimeStamp = DateTimeHelper.getTodayTimestamp();
/*     */ 
/* 340 */     authCenterDAO.cleanEncoderAuditUATiers(serverIPAddres, "IRD");
/*     */ 
/* 343 */     partition = BNCExternalClientAPIHelper.getDefaultPartition(this.bncExternalClientAPI);
/*     */ 
/* 346 */     affiliate = BNCExternalClientAPIHelper.getDefaultAffiliate(this.bncExternalClientAPI, partition);
/*     */ 
/* 349 */     ratingRegion = BNCExternalClientAPIHelper.getDefaultRatingRegion(this.bncExternalClientAPI);
/*     */ 
/* 352 */     location = BNCExternalClientAPIHelper.getDefaultLocation(this.bncExternalClientAPI, affiliate, partition, ratingRegion);
/*     */ 
/* 354 */     Collection<DCIIUnitFilter> IRDUnitAddresses = BNCExternalClientAPIHelper.queryAllIRDUnitAddresses(this.bncExternalClientAPI, location.getName(), affiliate.getName());
/*     */ 
/* 356 */     for (Iterator<DCIIUnitFilter> iteratorIRDUnitAddress = IRDUnitAddresses.iterator(); iteratorIRDUnitAddress.hasNext(); )
/*     */     {
/* 359 */       DCIIUnitFilter irdUnitAddress = (DCIIUnitFilter)iteratorIRDUnitAddress.next();
/*     */ 
/* 361 */       IRD queryIRD = IRD.getBlank();
/*     */ 
/* 363 */       if (!this.bncExternalClientAPI.irdExists(irdUnitAddress)) {
/*     */         continue;
/*     */       }
/* 366 */       queryIRD = this.bncExternalClientAPI.queryIRD(irdUnitAddress);
/*     */ 
/* 369 */       int[] tiers = queryIRD.getTierNumbers();
/*     */ 
/* 371 */       int templateID = queryIRD.getGeminiTemplateID();
/*     */ 
/* 373 */       for (int i = 0; i < tiers.length; i++) {
/* 374 */         authCenterDAO.SendData(serverIPAddres, null, irdUnitAddress.toDecimalString(false), "IRD", tiers[i], templateID, processedTimeStamp);
/*     */       }
/*     */ 
/*     */     }
/*     */ 
/* 379 */     authCenterDAO.cleanEncoderAuditUA(serverIPAddres, "IRD", processedTimeStamp);
/*     */ 
/* 382 */     authCenterDAO.releaseSQLConnection();
/*     */   }
/*     */ 
/*     */   public void queryAllMADUnitAddresses(String serverIPAddres) throws MotStatusException
/*     */   {
/* 387 */     BasePartition partition = null;
/* 388 */     Affiliate affiliate = null;
/* 389 */     Location location = null;
/* 390 */     RatingRegion ratingRegion = null;
/* 391 */     AuthCenterDAO authCenterDAO = new AuthCenterDAO();
/*     */ 
/* 393 */     if (this.bncExternalClientAPI == null) {
/* 394 */       return;
/*     */     }
/* 396 */     Timestamp processedTimeStamp = DateTimeHelper.getTodayTimestamp();
/*     */ 
/* 399 */     authCenterDAO.cleanEncoderAuditUATiers(serverIPAddres, "MD");
/*     */ 
/* 402 */     partition = BNCExternalClientAPIHelper.getDefaultPartition(this.bncExternalClientAPI);
/*     */ 
/* 405 */     affiliate = BNCExternalClientAPIHelper.getDefaultAffiliate(this.bncExternalClientAPI, partition);
/*     */ 
/* 408 */     ratingRegion = BNCExternalClientAPIHelper.getDefaultRatingRegion(this.bncExternalClientAPI);
/*     */ 
/* 411 */     location = BNCExternalClientAPIHelper.getDefaultLocation(this.bncExternalClientAPI, affiliate, partition, ratingRegion);
/*     */ 
/* 414 */     //Query all MAD Unit Addresses
        	  Collection<DCIIUnitFilter> madUnitAddresses = BNCExternalClientAPIHelper.queryAllMADUnitAddresses(bncExternalClientAPI, location.getName(), affiliate.getName());
/*     */ 
/* 416 */     for (Iterator<DCIIUnitFilter> iteratorMADUnitAddress = madUnitAddresses.iterator(); iteratorMADUnitAddress.hasNext(); )
/*     */     {
/* 419 */       DCIIUnitFilter madUnitAddress = (DCIIUnitFilter)iteratorMADUnitAddress.next();
/*     */ 
/* 422 */       Collection <DCIIUnitFilter>ACPUnitAddresses = BNCExternalClientAPIHelper.queryAllACPUnitAddresses(this.bncExternalClientAPI, madUnitAddress.toDecimalString());
/*     */ 
/* 424 */       for (Iterator<DCIIUnitFilter> iteratorACPUnitAddress = ACPUnitAddresses.iterator(); iteratorACPUnitAddress.hasNext(); )
/*     */       {
/* 427 */         DCIIUnitFilter acpUnitAddress = (DCIIUnitFilter)iteratorACPUnitAddress.next();
/*     */ 
/* 429 */         ACP queryACP = ACP.getBlank();
/*     */ 
/* 431 */         if (!this.bncExternalClientAPI.acpExists(acpUnitAddress)) {
/*     */           continue;
/*     */         }
/* 434 */         queryACP = this.bncExternalClientAPI.queryACP(acpUnitAddress);
/*     */ 
/* 437 */         int[] tiers = queryACP.getTierNumbers();
/* 438 */         String anchorUnitAddress = queryACP.getAnchorUnitAddress();
/*     */ 
/* 440 */         for (int i = 0; i < tiers.length; i++) {
/* 441 */           authCenterDAO.SendData(serverIPAddres, anchorUnitAddress, acpUnitAddress.toDecimalString(false), "MD", tiers[i], 0, processedTimeStamp);
/*     */         }
/*     */       }
/*     */ 
/*     */     }
/*     */ 
/* 447 */     authCenterDAO.cleanEncoderAuditUA(serverIPAddres, "MD", processedTimeStamp);
/*     */ 
/* 450 */     authCenterDAO.releaseSQLConnection();
/*     */   }
/*     */ }

/* Location:           C:\dev\BNCCenter\BNCAUCenterWS_Prod\BNCAUCenterWS\WEB-INF\classes\
 * Qualified Name:     com.hbolag.bncauthcenter.BNCAuthCenter
 * JD-Core Version:    0.6.0
 */