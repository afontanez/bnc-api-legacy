package com.hbolag.bncauthcenter.structures;

public class UnitData
{

	  private int encoderGroupID;
	  private String anchorUnitAddress;
	  private String unitAddress;
	  private int tierNumber;
	  private String tierNumberExt;
	  private String decoderModelType;
	  private String decoderModel;
	  private int templateID;
	  private int singleVCMID;
	  private String cableCode;
	  private int headEndCode;
	  private int countryID;
	  private String serialNumber;
	  private boolean isAuthorized;
	  private String isAuthorizedExt;
	  private boolean templateTierExtended;
	  private Integer transportStreamOutput;
	  private Boolean frontPanelEnabled;
	  private Boolean eMMStream;
	  private Integer eMMProviderId;

	  public int getEncoderGroupID()
	  {
	    return this.encoderGroupID;
	  }
	  public void setEncoderGroupID(int encoderGroupID) {
	    this.encoderGroupID = encoderGroupID;
	  }
	  public String getAnchorUnitAddress() {
	    return this.anchorUnitAddress;
	  }
	  public void setAnchorUnitAddress(String anchorUnitAddress) {
	    this.anchorUnitAddress = anchorUnitAddress;
	  }
	  public String getUnitAddress() {
	    return this.unitAddress;
	  }
	  public void setUnitAddress(String unitAddress) {
	    this.unitAddress = unitAddress;
	  }
	  public int getTierNumber() {
	    return this.tierNumber;
	  }
	  public void setTierNumber(int tierNumber) {
	    this.tierNumber = tierNumber;
	  }
	  public String[] getTierNumbersExt() {
	    return this.tierNumberExt.split("\\|");
	  }
	  public void setTierNumberExt(String tierNumber) {
	    this.tierNumberExt = tierNumber;
	  }
	  public String getDecoderModelType() {
	    return this.decoderModelType;
	  }
	  public void setDecoderModelType(String decoderModelType) {
	    this.decoderModelType = decoderModelType;
	  }
	  public String getDecoderModel() {
	    return this.decoderModel;
	  }
	  public void setDecoderModel(String decoderModel) {
	    this.decoderModel = decoderModel;
	  }
	  public int getTemplateID() {
	    return this.templateID;
	  }
	  public void setTemplateID(int templateID) {
	    this.templateID = templateID;
	  }
	  public int getSingleVCMID() {
	    return this.singleVCMID;
	  }
	  public void setSingleVCMID(int singleVCMID) {
	    this.singleVCMID = singleVCMID;
	  }
	  public String getCableCode() {
	    return this.cableCode;
	  }
	  public void setCableCode(String cableCode) {
	    this.cableCode = cableCode;
	  }
	  public int getHeadEndCode() {
	    return this.headEndCode;
	  }
	  public void setHeadEndCode(int headEndCode) {
	    this.headEndCode = headEndCode;
	  }
	  public int getCountryID() {
	    return this.countryID;
	  }
	  public void setCountryID(int countryID) {
	    this.countryID = countryID;
	  }
	  public String getSerialNumber() {
	    return this.serialNumber;
	  }
	  public void setSerialNumber(String serialNumber) {
	    this.serialNumber = serialNumber;
	  }
	  public boolean getIsAuthorized() {
	    return this.isAuthorized;
	  }
	  public void setIsAuthorized(boolean isAuthorized) {
	    this.isAuthorized = isAuthorized;
	  }
	  public String[] getIsAuthorizedExtTiers() {
	    return this.isAuthorizedExt.split("\\|");
	  }
	  public void setIsAuthorizedExt(String isAuthorizedExt) {
	    this.isAuthorizedExt = isAuthorizedExt;
	  }
	  public boolean isTemplateTierExtended() {
	    return this.templateTierExtended;
	  }
	  public void setTemplateTierExtended(boolean templateTierExtended) {
	    this.templateTierExtended = templateTierExtended;
	  }
	  public Integer getTransportStreamOutput() {
	    return this.transportStreamOutput;
	  }
	  public void setTransportStreamOutput(Integer transportStreamOutput) {
	    this.transportStreamOutput = transportStreamOutput;
	  }
	  public Boolean isFrontPanelEnabled() {
	    return this.frontPanelEnabled;
	  }
	  public void setFrontPanelEnabled(Boolean frontPanelEnabled) {
	    this.frontPanelEnabled = frontPanelEnabled;
	  }
	  public Boolean isEMMStream() {
	    return this.eMMStream;
	  }
	  public void setEMMStream(Boolean eMMStream) {
	    this.eMMStream = eMMStream;
	  }
	  public Integer getEMMProviderId() {
	    return this.eMMProviderId;
	  }
	  public void setEMMProviderId(Integer eMMProviderId) {
	    this.eMMProviderId = eMMProviderId;
	  }
	  public boolean isTransportStreamOutputValid() {
	    return this.transportStreamOutput != null;
	  }
	  public boolean isFrontPanelEnabledValid() {
	    return this.frontPanelEnabled != null;
	  }
	  public boolean isEMMStreamValid() {
	    return this.eMMStream != null;
	  }
	  public boolean isEMMProviderIdValid() {
	    return this.eMMProviderId != null;
	  }

	  public String toString() {
	    return "EncoderGroupID: " + Integer.toString(this.encoderGroupID) + " AnchorUnitAddress: " + this.anchorUnitAddress + " UnitAddress: " + this.unitAddress + " TierNumber: " + Integer.toString(this.tierNumber) + " TierNumberExt: " + this.tierNumberExt + 
	      " DecoderModelType: " + this.decoderModelType + " DecoderModel: " + this.decoderModel + " TemplateID: " + Integer.toString(this.templateID) + " SingleVCMID: " + Integer.toString(this.singleVCMID) + 
	      " CableCode: " + this.cableCode + " HeadEndCode: " + Integer.toString(this.headEndCode) + " CountryID: " + Integer.toString(this.countryID) + " SerialNumber: " + this.serialNumber + " IsAuthorized: " + this.isAuthorized + " IsAuthorizedExt: " + this.isAuthorizedExt + 
	      " TemplateTierExtended: " + this.templateTierExtended + " TransportStreamOutput: " + this.transportStreamOutput + " FrontPanelEnabled: " + this.frontPanelEnabled + " EMMStream: " + this.eMMStream + " EMMProviderId: " + this.eMMProviderId;
	  }
	}





