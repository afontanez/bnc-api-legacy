package com.hbolag.bncauthcenter;

import com.hbolag.bncauthcenter.structures.UnitData;
import com.motorola.refarch.infrastruct.errdiag.MotStatusException;

public abstract interface IBNCAuthCenterAPI
{
  public abstract String getBNCVersion()
    throws MotStatusException;

  public abstract void setIRD(UnitData paramUnitData)
    throws MotStatusException;

  public abstract void tripIRD(String paramString)
    throws MotStatusException;

  public abstract void deleteIRD(String paramString)
    throws MotStatusException;

  public abstract void queryAllIRDUnitAddresses(String paramString)
    throws MotStatusException;

  public abstract void setACP(UnitData paramUnitData)
    throws MotStatusException;

  public abstract void tripACP(String paramString)
    throws MotStatusException;

  public abstract void deleteACP(String paramString1, String paramString2)
    throws MotStatusException;

  public abstract void queryAllMADUnitAddresses(String paramString)
    throws MotStatusException;
}

/* Location:           C:\dev\BNCCenter\BNCAUCenterWS_Prod\BNCAUCenterWS\WEB-INF\classes\
 * Qualified Name:     com.hbolag.bncauthcenter.IBNCAuthCenterAPI
 * JD-Core Version:    0.6.0
 */