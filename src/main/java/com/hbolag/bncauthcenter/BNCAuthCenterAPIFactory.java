package com.hbolag.bncauthcenter;

import java.net.UnknownHostException;

import javax.naming.NamingException;
import javax.security.auth.login.LoginException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

//import org.apache.log4j.PropertyConfigurator;
import com.motorola.bnc.proxy.external.BNCExternalAPIFactory;
import com.motorola.bnc.proxy.external.IExternalClientAPI;
import com.motorola.bnc.proxy.external.IExternalClientAPI.BNCType;
//import com.motorola.refarch.infrastruct.errdiag.StatusMsgImpl;
import com.motorola.refarch.infrastruct.errdiag.MotStatusException;

public class BNCAuthCenterAPIFactory {
	
	private static final Logger logger = LogManager.getLogger(BNCAuthCenterAPIFactory.class);
	private static IExternalClientAPI bncExternalClientAPI = null; 
	private static IBNCAuthCenterAPI bncAuthCenterAPI = null;
	
	public static IBNCAuthCenterAPI getBNCAuthCenterAPI(String username, String password, String serverIPAddres) throws UnknownHostException, LoginException, NamingException, MotStatusException {
		
		logger.info("getBNCAuthCenterAPI");
		//Connect to BNC Server
		bncExternalClientAPI = BNCExternalAPIFactory.getExternalClientAPI(username, password, serverIPAddres);
		
		if (bncExternalClientAPI == null)
			return null;
		
		//BNC Type must not be split mux mgmt
		if (bncExternalClientAPI.getBNCType() == BNCType.SPLIT_MUX_MGMT)
		{
			logger.error("BNC Type should not be SPLIT_MUX_MGMT");
			return null;
		}
		logger.info("Connection established successfully...!");
		
		bncAuthCenterAPI = new BNCAuthCenter(bncExternalClientAPI);
		return  bncAuthCenterAPI; 
	}
	
	public static void releaseBNCAuthCenterAPI(IBNCAuthCenterAPI api) {
		logger.info("releaseBNCAuthCenterAPI");
		BNCExternalAPIFactory.releaseExternalClientAPI(bncExternalClientAPI);
		api = null;
	}
}
