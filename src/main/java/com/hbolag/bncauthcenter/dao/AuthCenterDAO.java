package com.hbolag.bncauthcenter.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AuthCenterDAO {
	private static final Logger logger = LogManager.getLogger(AuthCenterDAO.class);
	
	private static Connection sqlConnection = null;
    private static final String url 		= "jdbc:sqlserver://";
    private static final String portNumber 	= "1433";
    private String sqlServerName			= "localhost";
    private String sqlDatabaseName 			= "";
    private String sqlUserName				= "";
    private String sqlPassword 				= "";
    private boolean isConnectionOpen 		= false;

    public AuthCenterDAO(){
    	
    	//Get reference to SQL Database configuration
    	ResourceBundle rb = ResourceBundle.getBundle("connection_config");
    	
    	//Initialize sql variables
    	this.sqlServerName 		= rb.getString("server.name");
		this.sqlDatabaseName	= rb.getString("database.name");
		this.sqlUserName 		= rb.getString("user.name");
		this.sqlPassword 		= rb.getString("user.password");
    }
    
	private String getConnectionUrl(){
		return url + this.sqlServerName + ":" + portNumber + ";databaseName=" + this.sqlDatabaseName +";";
    }
	
	
		
	private boolean getSQLConnection() throws ClassNotFoundException, SQLException{

		if (!this.isConnectionOpen)
		{
			//Establish sql connection.
		    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		    sqlConnection = DriverManager.getConnection(getConnectionUrl(), this.sqlUserName, this.sqlPassword);
	        if(sqlConnection != null)
	        {
	        	logger.info("SQL Connection Successfully established!");
	        	isConnectionOpen = true;
	        	return true;
	        }
	        else
	        	return false;
		}
		else
			return true;
	}
	
	public void releaseSQLConnection(){
		
		if (sqlConnection != null)
		{
			try { 
				sqlConnection.close();
				isConnectionOpen = false;
				logger.info("SQL Connection Successfully released!"); 
			}
			catch(Exception e) 
			{ 
				logger.error("releaseSQLConnection: " + e.getMessage());
			}
		}
	}
	
	public void SendData(String serverIPAddres, String anchorUnitAddress,
			String unitAddress, String decoderModelType, int tierNumber, int templateID, Timestamp processedTimeStamp) {
		
        try {
        	        	
        	if (! this.getSQLConnection())
        		return;
        	
        	//Prepare sql statement
            CallableStatement cstmt = sqlConnection.prepareCall("{call dbo.HBO_i_EncoderAudit(?, ?, ?, ?, ?, ?, ?)}");

            //Set parameters
            cstmt.setNString("ServerIPAddress", serverIPAddres);
            cstmt.setNString("AnchorUnitAddress", anchorUnitAddress);
            cstmt.setNString("UnitAddress", unitAddress);
            cstmt.setNString("DecoderModelType", decoderModelType);
            cstmt.setInt("TierNumber", tierNumber);
            cstmt.setInt("TemplateID", templateID);
            cstmt.setTimestamp("ProcessedTimeStamp", processedTimeStamp);
            
            //Execute statement 
            cstmt.execute();
        }
        catch (Exception e) {
        	logger.error("SendData: " + e.getMessage());
        }
	}
	
	public void cleanEncoderAuditUA(String serverIPAddres, String decoderModelType, Timestamp processedTimeStamp){
				 
		try {
        	if (!this.getSQLConnection())
        		return;
        	
             //Prepare sql statement
             CallableStatement cstmt = sqlConnection.prepareCall("{call dbo.HBO_d_EncoderAuditUA(?, ?, ?)}");

             //Set parameters
             cstmt.setNString("ServerIPAddress", serverIPAddres);
             cstmt.setNString("DecoderModelType", decoderModelType);
             cstmt.setTimestamp("ProcessedTimeStamp", processedTimeStamp);
             
             //Execute statement
             cstmt.executeUpdate();
        }
        catch (Exception e) {
        	logger.error("cleanEncoderAuditUA: " + e.getMessage());
        }
	}
	
	public void cleanEncoderAuditUATiers(String serverIPAddres, String decoderModelType){
		
		try {
        	if (!this.getSQLConnection())
        		return;
        	
             //Prepare sql statement
             CallableStatement cstmt = sqlConnection.prepareCall("{call dbo.HBO_d_EncoderAuditUATiers(?, ?)}");

             //Set parameters
             cstmt.setNString("ServerIPAddress", serverIPAddres);
             cstmt.setNString("DecoderModelType", decoderModelType);

             //Execute statement
             cstmt.executeUpdate();
        }
        catch (Exception e) {
        	logger.error("cleanEncoderAuditUATiers: " + e.getMessage());
        }
	}
}
