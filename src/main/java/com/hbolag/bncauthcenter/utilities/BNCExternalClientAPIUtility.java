/*    */ package com.hbolag.bncauthcenter.utilities;
/*    */ 
/*    */ import com.motorola.bnc.common.shared.structures.Affiliate;
/*    */ import com.motorola.bnc.common.shared.structures.AffiliateGroup;
/*    */ import com.motorola.bnc.common.shared.structures.Location;
/*    */ import com.motorola.bnc.common.shared.structures.RatingRegion;
/*    */ import com.motorola.bnc.proxy.external.IExternalClientAPI;
/*    */ import com.motorola.refarch.infrastruct.errdiag.MotStatusException;
/*    */ import com.motorola.refarch.infrastruct.gendata.BasePartition;
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ 
/*    */ public class BNCExternalClientAPIUtility
/*    */ {
/* 13 */   private static String AFFILIATE_GROUP_NAME = "HBO-LAG";
/* 14 */   private static String AFFILIATE_NAME = "HBOLAG-CAS";
/* 15 */   private static String LOCATION_NAME = "SUNRISE-CAS";
/* 16 */   public static int EASTERN_STANDART_TIME = 17;
/*    */ 
/*    */   private static AffiliateGroup getHBOLAGAffiliateGroup(BasePartition defaultBasePartition)
/*    */   {
/* 20 */     AffiliateGroup hbolagAffiliateGroup = AffiliateGroup.getBlank();
/*    */ 
/* 22 */     hbolagAffiliateGroup.setName(AFFILIATE_GROUP_NAME);
/* 23 */     hbolagAffiliateGroup.setDBUID(0);
/* 24 */     hbolagAffiliateGroup.setInDB(true);
/* 25 */     hbolagAffiliateGroup.setPtNumber(defaultBasePartition.getPTNumber());
/*    */ 
/* 27 */     return hbolagAffiliateGroup;
/*    */   }
/*    */ 
/*    */   private static Affiliate setHBOLAGAffiliate(AffiliateGroup hbolagAffiliateGroup, BasePartition defaultBasePartition)
/*    */   {
/* 32 */     List listAffiliateGroup = new ArrayList();
/*    */ 
/* 34 */     listAffiliateGroup.add(hbolagAffiliateGroup);
/*    */ 
/* 36 */     Affiliate hbolagAffiliate = Affiliate.getBlank();
/*    */ 
/* 38 */     hbolagAffiliate.setName(AFFILIATE_NAME);
/* 39 */     hbolagAffiliate.setAffiliateGroups(listAffiliateGroup);
/* 40 */     hbolagAffiliate.setContactName("Luis Serfaty");
/* 41 */     hbolagAffiliate.setAddress1("13801 N.W. 14th St.");
/* 42 */     hbolagAffiliate.setCity("Sunrise");
/* 43 */     hbolagAffiliate.setCountry("United States");
/* 44 */     hbolagAffiliate.setPostalCode("33323");
/* 45 */     hbolagAffiliate.setState("FL");
/* 46 */     hbolagAffiliate.setDBUID(0);
/* 47 */     hbolagAffiliate.setInDB(true);
/* 48 */     hbolagAffiliate.setPtNumber(defaultBasePartition.getPTNumber());
/*    */ 
/* 50 */     return hbolagAffiliate;
/*    */   }
/*    */ 
/*    */   private static Location setHBOLAGLocation(BasePartition defaultBasePartition, RatingRegion defaultRatingRegion, Affiliate defaultAffiliate)
/*    */   {
/* 55 */     Location hbolagLocation = Location.getBlank();
/* 56 */     hbolagLocation.setName(LOCATION_NAME);
/* 57 */     hbolagLocation.setContactName("Luis Serfaty");
/* 58 */     hbolagLocation.setAddress1("13801 N.W. 14th St.");
/* 59 */     hbolagLocation.setCity("Sunrise");
/* 60 */     hbolagLocation.setCountry("United States");
/* 61 */     hbolagLocation.setPostalCode("33323");
/* 62 */     hbolagLocation.setState("FL");
/* 63 */     hbolagLocation.setDBUID(0);
/* 64 */     hbolagLocation.setInDB(true);
/* 65 */     hbolagLocation.setAffiliate(defaultAffiliate);
/*    */ 
/* 67 */     hbolagLocation.setTimeZone((byte)EASTERN_STANDART_TIME);
/* 68 */     hbolagLocation.setRatingRegion(defaultRatingRegion);
/* 69 */     hbolagLocation.setPtNumber(defaultBasePartition.getPTNumber());
/*    */ 
/* 71 */     return hbolagLocation;
/*    */   }
/*    */ 
/*    */   public static Affiliate setDefaultAffiliate(IExternalClientAPI bncExternalClientAPI, BasePartition defaultBasePartition) throws MotStatusException {
/* 75 */     AffiliateGroup hbolagAffiliateGroup = null;
/*    */ 
/* 77 */     Affiliate foundAffiliate = BNCExternalClientAPIHelper.queryAffiliate(bncExternalClientAPI, AFFILIATE_NAME);
/* 78 */     if (foundAffiliate == null)
/*    */     {
/* 80 */       if (!bncExternalClientAPI.affiliateGroupExists(AFFILIATE_GROUP_NAME)) {
/* 81 */         BNCExternalClientAPIHelper.storeAffiliateGroup(bncExternalClientAPI, getHBOLAGAffiliateGroup(defaultBasePartition));
/*    */       }
/* 83 */       if (bncExternalClientAPI.affiliateGroupExists(AFFILIATE_GROUP_NAME)) {
/* 84 */         hbolagAffiliateGroup = BNCExternalClientAPIHelper.queryAffiliateGroup(bncExternalClientAPI, AFFILIATE_GROUP_NAME);
/*    */       }
/* 86 */       return BNCExternalClientAPIHelper.storeAffiliate(bncExternalClientAPI, setHBOLAGAffiliate(hbolagAffiliateGroup, defaultBasePartition));
/*    */     }
/*    */ 
/* 89 */     return foundAffiliate;
/*    */   }
/*    */ 
/*    */   public static Location setDefaultLocation(IExternalClientAPI bncExternalClientAPI, BasePartition defaultBasePartition, RatingRegion defaultRatingRegion, Affiliate defaultAffiliate) throws MotStatusException
/*    */   {
/* 94 */     Location foundLocation = BNCExternalClientAPIHelper.queryLocation(bncExternalClientAPI, LOCATION_NAME, AFFILIATE_NAME);
/* 95 */     if (foundLocation == null) {
/* 96 */       return BNCExternalClientAPIHelper.storeLocation(bncExternalClientAPI, setHBOLAGLocation(defaultBasePartition, defaultRatingRegion, defaultAffiliate));
/*    */     }
/* 98 */     return foundLocation;
/*    */   }
/*    */ }

/* Location:           C:\dev\BNCCenter\BNCAUCenterWS_Prod\BNCAUCenterWS\WEB-INF\classes\
 * Qualified Name:     com.hbolag.bncauthcenter.utilities.BNCExternalClientAPIUtility
 * JD-Core Version:    0.6.0
 */