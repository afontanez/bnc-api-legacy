package com.hbolag.bncauthcenter.utilities;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hbolag.bncauthcenter.structures.UnitData;
import com.motorola.bnc.common.shared.structures.ACP;
import com.motorola.bnc.common.shared.structures.Affiliate;
import com.motorola.bnc.common.shared.structures.AutoHdSdAspectRatioConversionMethodEnumType;
import com.motorola.bnc.common.shared.structures.Channel;
import com.motorola.bnc.common.shared.structures.DecoderModel;
import com.motorola.bnc.common.shared.structures.DecoderModelNumber;
import com.motorola.bnc.common.shared.structures.DecoderModelType;
import com.motorola.bnc.common.shared.structures.DecoderPreset;
import com.motorola.bnc.common.shared.structures.DefaultSdAspectRatioEnumType;
import com.motorola.bnc.common.shared.structures.HdOutputVideoResolutionFrameRateEnumType;
import com.motorola.bnc.common.shared.structures.HdReEncoderData;
import com.motorola.bnc.common.shared.structures.IRD;
import com.motorola.bnc.common.shared.structures.Location;
import com.motorola.bnc.common.shared.structures.MAD;
import com.motorola.bnc.common.shared.structures.MiscData;
import com.motorola.bnc.common.shared.structures.Mpeg2SdOutputVideoResolutionEnumType;
import com.motorola.bnc.common.shared.structures.MultiAcpIRD;
import com.motorola.bnc.common.shared.structures.RatingRegion;
import com.motorola.bnc.common.shared.structures.SDIOutputControlType;
import com.motorola.bnc.common.shared.structures.SdReEncoderData;
import com.motorola.bnc.common.shared.structures.Transcoder;
import com.motorola.bnc.common.shared.structures.TranscoderDto;
import com.motorola.bnc.common.shared.structures.TranscoderHDStatMux;
import com.motorola.refarch.infrastruct.gendata.BaseOperatorGroup;
import com.motorola.refarch.infrastruct.gendata.BasePartition;
import com.motorola.refarch.infrastruct.gendata.BaseRegion;
import com.motorola.refarch.infrastruct.gendata.BaseTier;

public class BNCAuthCenterHelper
{
  private static Logger logger = LogManager.getLogger(BNCAuthCenterHelper.class);

  public static IRD populateIRD(UnitData unitData, BasePartition partition, Affiliate affiliate, Location location, BaseOperatorGroup OperatorGroup, BaseRegion region, RatingRegion ratingRegion, DecoderModel decoderModel, List<Channel> virtualChannels)
  {
    IRD newIRD = IRD.getBlank();

    newIRD.setPTID(partition.getPTNumber());
  
    newIRD.setLocation(location);

    newIRD.setOG(OperatorGroup);

    newIRD.setRegion(region);

    newIRD.setRatingRegion(ratingRegion);

    newIRD.setUnitAddress(unitData.getUnitAddress());

    newIRD.setModel(decoderModel);

    newIRD.setGeminiTemplateID(unitData.getTemplateID());

    if (isDecoderModelNumberDSR_640N(decoderModel)) {
      newIRD.setTiers(setTiers_640N(unitData.getTierNumbersExt(), unitData.getIsAuthorizedExtTiers()));
    }
    else {
      newIRD.setTiers(setTiers(unitData.getTierNumber(), unitData.getTemplateID(), unitData.isTemplateTierExtended()));
    }
    
    newIRD.setTimeZone((byte)BNCExternalClientAPIUtility.EASTERN_STANDART_TIME);

    if (isDecoderModelNumberDSR_4460(decoderModel))
    {
      if (unitData.isTransportStreamOutputValid()) {
        newIRD.setTransportStreamOutput((byte)unitData.getTransportStreamOutput().intValue());
      }
      if (unitData.isFrontPanelEnabledValid()) {
        newIRD.setFrontPanelEnabled(unitData.isFrontPanelEnabled().booleanValue());
      }
//      if (unitData.isEMMStreamValid()) {
//        newIRD.setEMMStreamRelease((byte)(unitData.isEMMStream().booleanValue() ? 1 : 0));
//        if (unitData.isEMMProviderIdValid()) {
//        	
//        	if (isDecoderModelNumberDSR_4460(decoderModel)) {        	
//        		SDIOutputControlType sdiOutputControlType = mappEmmProviderIdToSdiOutput(unitData.getEMMProviderId().intValue());
//        		newIRD.setSDIOutputControl(sdiOutputControlType);        	
//        	}
//        	else {
//        		newIRD.setEMMProviderID(unitData.getEMMProviderId().intValue());
//        	}
//        }
//      }
    	if (isDecoderModelNumberDSR_4460(newIRD.getModel())) {
        	SDIOutputControlType sdiOutputControlType = mappEmmProviderIdToSdiOutput(unitData.getEMMProviderId());
        	newIRD.setSDIOutputControl(sdiOutputControlType);
      	}      
    	newIRD.setEMMProviderID(0);
    	newIRD.setEMMStreamRelease((byte)0);
        
    }
    else if (isDecoderModelNumberDSR_6300(decoderModel)) {
      logger.info("populateIRD - Set TransportStreamOutput and FrontPanelEnabled for model: " + decoderModel.getKey());
      if (unitData.isTransportStreamOutputValid()) {
        newIRD.setTransportStreamOutput((byte)unitData.getTransportStreamOutput().intValue());
      }
      if (unitData.isFrontPanelEnabledValid()) {
        newIRD.setFrontPanelEnabled(unitData.isFrontPanelEnabled().booleanValue());
      }
    }

    newIRD.setSerialNumber(unitData.getSerialNumber());
    newIRD.setName(unitData.getSerialNumber());
    
    if (isDecoderModelNumberDSR_640N(decoderModel)) {
      logger.info("populateIRD -  Model: " + newIRD.getModel().getKey());
      if (unitData.isTransportStreamOutputValid()) {
        newIRD.setTransportStreamOutput((byte)unitData.getTransportStreamOutput().intValue());
      }
      newIRD.setInterfacePasswordReset((byte) 1);

      byte hdStatMuxGroupEnabled = (byte)getHdStatMuxGroupEnabled(unitData.getIsAuthorizedExtTiers());
      newIRD.setHdStatMuxGroupEnabled(hdStatMuxGroupEnabled);
      logger.info("setHdStatMuxGroupEnabled: " + Byte.toString(newIRD.getHdStatMuxGroupEnabled()));

      newIRD.setHdStatMuxGroupBitRateKbps(35000);
      newIRD.setMultiChannelRetuneEnabled(true);

      int numTranscoders = getNumTranscodersForDecoderModel(decoderModel);      
      List<Transcoder> transcoders = setupTranscoders_640N(newIRD.getName(), virtualChannels, unitData.getIsAuthorizedExtTiers(), hdStatMuxGroupEnabled, null, numTranscoders);
      newIRD.getTranscoders().clear();
      newIRD.getTranscoders().addAll(transcoders);
    }
    else 
    {
      byte byteValue = 1;	
      newIRD.setAcqRecoveryMode(byteValue);
//      newIRD.setSDIOutput(byteValue);
      newIRD.setAESOutput(byteValue);
      newIRD.setTierOn(true);
      newIRD.setMiscFields(setMiscFields(unitData.getCableCode(), Integer.toString(unitData.getHeadEndCode()), "", ""));
      
      int numTranscoders = getNumTranscodersForDecoderModel(decoderModel); 
      List<Transcoder> transcoders = setupBlankTranscoders(numTranscoders, isDecoderModelNumberDSR_6050(decoderModel));
      newIRD.getTranscoders().clear();
      newIRD.getTranscoders().addAll(transcoders);

    }
    logger.info("populateIRD - return newIRD");
    
    return newIRD;
  }

  public static IRD populateIRD(UnitData unitData, IRD existingIRD, List<Channel> virtualChannels) {
    logger.info("Entering populateIRD for existingIRD");
        
    existingIRD.clearTiers();
    if (isDecoderModelNumberDSR_640N(existingIRD.getModel())) {
      existingIRD.setTiers(setTiers_640N(unitData.getTierNumbersExt(), unitData.getIsAuthorizedExtTiers()));
    }
    else {
      existingIRD.setTiers(setTiers(unitData.getTierNumber(), unitData.getTemplateID(), unitData.isTemplateTierExtended()));
    }

    existingIRD.setGeminiTemplateID(unitData.getTemplateID());
    if (isDecoderModelNumberDSR_4460(existingIRD.getModel())) {
      logger.info("Processing Decoder Model: " + existingIRD.getModel().toString());
      if (unitData.isTransportStreamOutputValid()) {
        existingIRD.setTransportStreamOutput((byte)unitData.getTransportStreamOutput().intValue());
      }
      if (unitData.isFrontPanelEnabledValid()) {
        existingIRD.setFrontPanelEnabled(unitData.isFrontPanelEnabled().booleanValue());
      }
//      if (unitData.isEMMStreamValid()) {
//        existingIRD.setEMMStreamRelease((byte)(unitData.isEMMStream().booleanValue() ? 1 : 0));
//		
//		if (unitData.isEMMProviderIdValid()) {
//			
//			if (isDecoderModelNumberDSR_4460(existingIRD.getModel())) {
//	        	SDIOutputControlType sdiOutputControlType = mappEmmProviderIdToSdiOutput(unitData.getEMMProviderId().intValue());
//	        	existingIRD.setSDIOutputControl(sdiOutputControlType);
//			}
//			else {
//				existingIRD.setEMMProviderID(unitData.getEMMProviderId().intValue());
//			}
//		}
//      }
  	if (isDecoderModelNumberDSR_4460(existingIRD.getModel())) {
    	SDIOutputControlType sdiOutputControlType = mappEmmProviderIdToSdiOutput(unitData.getEMMProviderId());
    	existingIRD.setSDIOutputControl(sdiOutputControlType);
  	}      
    existingIRD.setEMMProviderID(0);
    existingIRD.setEMMStreamRelease((byte)0);
  	
  	
    }
    else if (isDecoderModelNumberDSR_6300(existingIRD.getModel())) {
      logger.info("Processing Decoder Model: " + existingIRD.getModel().toString());
      if (unitData.isTransportStreamOutputValid()) {
        existingIRD.setTransportStreamOutput((byte)unitData.getTransportStreamOutput().intValue());
      }
      if (unitData.isFrontPanelEnabledValid()) {
        existingIRD.setFrontPanelEnabled(unitData.isFrontPanelEnabled().booleanValue());
      }
    }

    existingIRD.setSerialNumber(unitData.getSerialNumber());
    existingIRD.setName(unitData.getSerialNumber());
    
    if (isDecoderModelNumberDSR_640N(existingIRD.getModel())) {      

      if (unitData.isTransportStreamOutputValid()) {
        existingIRD.setTransportStreamOutput((byte)unitData.getTransportStreamOutput().intValue());
      }
      existingIRD.setInterfacePasswordReset((byte) 1);

      byte hdStatMuxGroupEnabled = (byte)getHdStatMuxGroupEnabled(unitData.getIsAuthorizedExtTiers());
      existingIRD.setHdStatMuxGroupEnabled(hdStatMuxGroupEnabled);

      logger.info("setHdStatMuxGroupEnabled: " + Byte.toString(existingIRD.getHdStatMuxGroupEnabled()));

      existingIRD.setHdStatMuxGroupBitRateKbps(35000);
      existingIRD.setMultiChannelRetuneEnabled(true);
      
      int numTranscoders = getNumTranscodersForDecoderModel(existingIRD.getModel());
      List<Transcoder> transcoders = setupTranscoders_640N(existingIRD.getName(), virtualChannels, unitData.getIsAuthorizedExtTiers(), hdStatMuxGroupEnabled, existingIRD.getTranscoders(), numTranscoders);
      existingIRD.getTranscoders().clear();
      existingIRD.getTranscoders().addAll(transcoders);
    }
//    else {
//    	int numTranscoders = getNumTranscodersForDecoderModel(existingIRD.getModel());
//    	List<Transcoder> transcoders = setupBlankTranscoders(numTranscoders);
//    	existingIRD.getTranscoders().clear();
//    	existingIRD.getTranscoders().addAll(transcoders);
//    }

    if ((!isDecoderModelNumberDSR_640N(existingIRD.getModel())) && (!isDecoderModelNumberDSR_4201_Or_6050(existingIRD.getModel()))) {
      logger.info("Setting MiscFields for Model: " + existingIRD.getModel().getKey());
      existingIRD.setMiscFields(setMiscFields(unitData.getCableCode(), Integer.toString(unitData.getHeadEndCode()), "", ""));
    }
    logger.info("populateIRD - return existingIRD");
    
    
    return existingIRD;
  }

  private static List<Transcoder> setupBlankTranscoders(int numTranscoders, boolean isDSR6050) {
	  
	  	List<Transcoder> transcoders = new ArrayList<Transcoder>();
	  	
	  	int x = 0;
		while (x++ < numTranscoders) {
		    	
			Transcoder blankTranscoder = TranscoderDto.getBlank();
			blankTranscoder.setTranscoderIndex(x);
			
			if (isDSR6050) {
				blankTranscoder.setHdReEncoderData(setHdReEncoderData(1));
			}			
			
			transcoders.add(blankTranscoder);			
		}
		
		return transcoders;
		
  }
  
  private static int getNumTranscodersForDecoderModel(DecoderModel decoderModel) {
	  
	  int numTranscoders = 0;//4410 have no transcoders
	  if (decoderModel.isModelNumber(DecoderModelNumber.DSR_6050) || decoderModel.isModelNumber(DecoderModelNumber.DSR_4460) || decoderModel.isModelNumber(DecoderModelNumber.DSR_4201)) numTranscoders = 1;
	  else if (decoderModel.isModelNumber(DecoderModelNumber.DSR_6300)) numTranscoders = 3;
	  else if (decoderModel.isModelNumber(DecoderModelNumber.DSR_6401)) numTranscoders = 1;
	  else if (decoderModel.isModelNumber(DecoderModelNumber.DSR_6402)) numTranscoders = 2;
	  else if (decoderModel.isModelNumber(DecoderModelNumber.DSR_6403)) numTranscoders = 3;
	  else if (decoderModel.isModelNumber(DecoderModelNumber.DSR_6404)) numTranscoders = 4;
	  
	  return numTranscoders;
  }
  
  public static ACP populateACP(UnitData unitData, BasePartition partition, MAD mad, BaseOperatorGroup OperatorGroup, BaseRegion region) {
    ACP newACP = ACP.getBlank();

    newACP.setPT(partition.getPTNumber());

    newACP.setMAD(mad);

    newACP.setOG(OperatorGroup);

    newACP.setRegion(region);

    newACP.setAnchorUnitAddress(unitData.getAnchorUnitAddress());

    newACP.setUnitAddress(unitData.getUnitAddress());

    if (unitData.getIsAuthorized()) {
      newACP.setTiers(setTiers(unitData.getTierNumber(), 0, false));
    }

    newACP.setName(unitData.getUnitAddress());
    newACP.setTierOn(true);
    newACP.setMiscFields(setMiscFields(unitData.getCableCode(), Integer.toString(unitData.getHeadEndCode()), "", ""));
    return newACP;
  }

  public static ACP populateACP(UnitData unitData, ACP existingACP)
  {
    existingACP.clearTiers();
    if (unitData.getIsAuthorized()) {
      existingACP.setTiers(setTiers(unitData.getTierNumber(), 0, false));
    }

    existingACP.setMiscFields(setMiscFields(unitData.getCableCode(), Integer.toString(unitData.getHeadEndCode()), "", ""));
    return existingACP;
  }

  public static MAD populateMAD(UnitData unitData, BasePartition partition, Location location, BaseOperatorGroup OperatorGroup, DecoderModel decoderModel) {
    MAD newMAD = null;

    if ((DecoderModelType.IRT.getName() == unitData.getDecoderModelType()) || (DecoderModelType.MPS.getName() == unitData.getDecoderModelType()))
      newMAD = MAD.getBlank();
    else if (DecoderModelType.MD.getName() == unitData.getDecoderModelType())
      newMAD = MultiAcpIRD.getBlankMD();
    else {
      return null;
    }
    newMAD.setPT(partition.getPTNumber());
    newMAD.setLocation(location);
    newMAD.setOG(OperatorGroup);
    newMAD.setSerialNumber(unitData.getSerialNumber());
    newMAD.setUnitAddress(unitData.getAnchorUnitAddress());
    newMAD.setName(unitData.getAnchorUnitAddress());
    newMAD.setModel(decoderModel);

    return newMAD;
  }

  public static void displayTranscoders(List<Transcoder> transcoders) {
    logger.info("Transcoders List");
    for (Transcoder transcoder : transcoders)
      logger.info("Transcoder: " + transcoder.toString());
  }

  public static void displayVirtualChannels(List<Channel> channels)
  {
    logger.info("Virtual Channel List");
    for (Channel channel : channels)
      logger.info("Virtual Channel: " + channel.getInfo());
  }

  private static List<MiscData> setMiscFields(String field1, String field2, String field3, String field4)
  {
    List<MiscData> miscFields = new ArrayList<MiscData>();

    for (int i = 0; i < 3; i++) {
      MiscData miscData = new MiscData(i);
      if (i == 0)
        miscData.setValue(field1);
      else if (i == 1)
        miscData.setValue(field2);
      else if (i == 2)
        miscData.setValue(field3);
      else if (i == 3) {
        miscData.setValue(field4);
      }
      miscFields.add(miscData);
    }
    return miscFields;
  }

  private static List<BaseTier> setTiers(int tierNumber, int templateID, boolean templateTierExtended) {
    List<BaseTier> tiers = new ArrayList<BaseTier>();
    int[] iTiers = (int[])null;
    if (templateID == 0)
    {
      if (tierNumber == 100)
      {
        iTiers = getArrayOfTiers(tierNumber, 0, false);
        for (int i = 0; i < iTiers.length; i++)
        {
          BaseTier tier = new BaseTier(iTiers[i]);
          tier.setNumber(iTiers[i]);
          tiers.add(tier);
        }
      }
      else
      {
        BaseTier theTier = new BaseTier(tierNumber);
        theTier.setNumber(tierNumber);
        tiers.add(theTier);
      }
    }
    else
    {
      iTiers = getArrayOfTiers(tierNumber, templateID, templateTierExtended);
      for (int i = 0; i < iTiers.length; i++)
      {
        BaseTier tier = new BaseTier(iTiers[i]);
        tier.setNumber(iTiers[i]);
        tiers.add(tier);
      }
    }
    return tiers;
  }

//  private static List<BaseTier> setTiers(String[] tierNumbers) {
//    List<BaseTier> tiers = new ArrayList<BaseTier>();
//
//    int[] iTiers = new int[tierNumbers.length];
//    int j = 0;
//    String[] arrayOfString = tierNumbers;
//    j = tierNumbers.length;
//    for (int i = 0; i < j; i++) { 
//      String str = arrayOfString[i];
//      iTiers[(i)] = Integer.parseInt(str);
//    }
//    for (int i = 0; i < iTiers.length; i++) {
//      int tierNumber = iTiers[i];
//      if (tierNumber > 0) {
//        BaseTier tier = new BaseTier(tierNumber);
//        tier.setNumber(tierNumber);
//        tiers.add(tier);
//      }
//    }
//    return tiers;
//  }

  //public for junit test
  public static List<BaseTier> setTiers_640N(String[] tierNumbers, String[] areTiersAuthorized) {// no crying  >:[    tears are not authorized!!!!
	  
	    List<BaseTier> tiers = new ArrayList<BaseTier>();
	    
	    if (tierNumbers != null && tierNumbers.length > 0 && areTiersAuthorized != null && areTiersAuthorized.length > 0) {
	    
	    	for (int i = 0; i < tierNumbers.length; i++) {
	    		try {
			    	int tierNumber = Integer.parseInt(tierNumbers[i]);
			    	boolean isTierAuthorized = areTiersAuthorized[i].trim().equals("1")?true:false;
			    	
			    	if (isTierAuthorized) {
				    	BaseTier tier = new BaseTier(tierNumber);
				        tier.setNumber(tierNumber);
				        tiers.add(tier);
			    	}
	    		} catch (Exception e) {
	    			//on exception assume tier is not authorized and continue looping
	    		}
	    	}
	    }
	    
	    return tiers;
	    
	    
	  }
  
  //public for junit test
  public static int[] getArrayOfTiers(int tierNumber, int templateID, boolean templateTierExtended) {
    int[] iTiers = new int[3];
    boolean IsFutureUse = false;

    if (templateID == 0)
    {
      if (tierNumber == 100)
      {
        iTiers = new int[101];
        for (int i = 0; i <= iTiers.length - 1; i++)
        {
          iTiers[i] = i;
        }
      }
    }
    else
    {
      String strTierNumber = Integer.toString(tierNumber);
      if (!templateTierExtended)
      {
        int pos;
        if (Integer.parseInt(strTierNumber.substring(strTierNumber.length() - 3)) == 400)
        {
          IsFutureUse = true;
          pos = strTierNumber.length() - 3;
          iTiers[2] = Integer.parseInt(strTierNumber.substring(pos));

          if (Integer.parseInt(strTierNumber.substring(strTierNumber.length() - 6, strTierNumber.length() - 3)) == 400)
          {
            IsFutureUse = true;
            pos = strTierNumber.length() - 6;
            iTiers[1] = Integer.parseInt(strTierNumber.substring(pos, strTierNumber.length() - 3));

            if (strTierNumber.length() == 7)
            {
              pos--;
              iTiers[0] = Integer.parseInt(strTierNumber.substring(pos, 1));
            }
            else if (strTierNumber.length() == 8)
            {
              pos -= 2;
              iTiers[0] = Integer.parseInt(strTierNumber.substring(pos, 2));
            }
            return iTiers;
          }
        }
        else
        {
          pos = strTierNumber.length() - 2;
          iTiers[2] = Integer.parseInt(strTierNumber.substring(pos));
        }
        int prePos = pos;
        pos -= 2;
        iTiers[1] = Integer.parseInt(strTierNumber.substring(pos, prePos));
        prePos = pos;
        if ((strTierNumber.length() >= 7) || ((strTierNumber.length() == 6) && (!IsFutureUse)))
        {
          pos -= 2;
          iTiers[0] = Integer.parseInt(strTierNumber.substring(pos, prePos));
        }
        else
        {
          pos--;
          iTiers[0] = Integer.parseInt(strTierNumber.substring(pos, prePos));
        }
      }
      else {
        int index = 0;
        strTierNumber = PrependZeros(strTierNumber);
        while (index < strTierNumber.length()) {
          if (index == 0)
          {
            iTiers[0] = Integer.parseInt(strTierNumber.substring(index, Math.min(index + 3, strTierNumber.length())));
          }
          else if (index == 3) {
            iTiers[1] = Integer.parseInt(strTierNumber.substring(index, Math.min(index + 3, strTierNumber.length())));
          }
          else if (index == 6) {
            iTiers[2] = Integer.parseInt(strTierNumber.substring(index, Math.min(index + 3, strTierNumber.length())));
          }
          index += 3;
        }
        logger.info("Array Of Tiers as follows:");
        for (int i = 0; i <= iTiers.length - 1; i++)
        {
          logger.info("Tier" + Integer.toString(i) + ": " + Integer.toString(iTiers[i]));
        }
      }
    }
    return iTiers;
  }

  private static String PrependZeros(String tierNumber) {
    int tierLength = tierNumber.length();

    if (tierLength == 9) {
      return tierNumber;
    }
    int zerosCount = 9 - tierLength;
    for (int i = 0; i < zerosCount; i++) {
      tierNumber = "0" + tierNumber;
    }
    return tierNumber;
  }

  public static boolean isDecoderModelNumberDSR_4201_Or_6050(DecoderModel decoderModel)
  {
    return (decoderModel.isModelNumber(DecoderModelNumber.DSR_4201)) || (decoderModel.isModelNumber(DecoderModelNumber.DSR_6050));
  }

  public static boolean isDecoderModelNumberDSR_6050(DecoderModel decoderModel)
  {
    return decoderModel.isModelNumber(DecoderModelNumber.DSR_6050);
  }

  public static boolean isDecoderModelNumberDSR_4460(DecoderModel decoderModel)
  {
    return decoderModel.isModelNumber(DecoderModelNumber.DSR_4460);
  }

  public static boolean isDecoderModelNumberDSR_6300(DecoderModel decoderModel)
  {
    return decoderModel.isModelNumber(DecoderModelNumber.DSR_6300);
  }

  public static boolean isDecoderModelNumberDSR_640N(DecoderModel decoderModel)
  {
    return (decoderModel.isModelNumber(DecoderModelNumber.DSR_6401)) || (decoderModel.isModelNumber(DecoderModelNumber.DSR_6402)) || (decoderModel.isModelNumber(DecoderModelNumber.DSR_6403)) || (decoderModel.isModelNumber(DecoderModelNumber.DSR_6404));
  }

//  private static List<Transcoder> setTranscodersForDSR_4201_Or_6050()
//  {
//    List<Transcoder> transcoders = new ArrayList<Transcoder>();
//    Transcoder transcoder = TranscoderDto.getBlank();
//
//    transcoder.setHdReEncoderData(setHdReEncoderData(1));
//    transcoders.add(transcoder);
//    return transcoders;
//  }

  private static int getHdStatMuxGroupEnabled(String[] isAuthorizedExtTiers) {
    int totalAuthorizedTranscoders = 0;
    for (int i = 0; i < isAuthorizedExtTiers.length; i++) {
      int enabled = Integer.parseInt(isAuthorizedExtTiers[i]);
      if (enabled == 1)
        totalAuthorizedTranscoders++;
    }
    return totalAuthorizedTranscoders >= 3 ? 1 : 0;
  }

  private static List<Transcoder> setupTranscoders_640N(String name, List<Channel> virtualChannels, String[] isAuthorizedExtTiers, byte hdStatMuxGroupEnabled, List<Transcoder> currentTranscoders, int numTranscoders)
  {

    List<Transcoder> transcoders = new ArrayList<Transcoder>();

    for (int i = 0; i < virtualChannels.size(); i++) {
    	
    	Channel virtualChannel = (Channel)virtualChannels.get(i);
    	int enabled = Integer.parseInt(isAuthorizedExtTiers[i]);
    	boolean statMuxGroupIncluded = false;

    	if (hdStatMuxGroupEnabled == 1) {
    		statMuxGroupIncluded = enabled == 1;
    	}
    	
    	Transcoder transcoder = TranscoderDto.getBlank();
    	if ((currentTranscoders == null) || (currentTranscoders.size() == 0)) {
    		    		
    		transcoder.setTranscoderIndex(i + 1);
    		transcoder.setName(name + "-" + Integer.toString(i + 1));
    		
    		transcoder.setHdReEncoderData(setHdReEncoderData(enabled));    		    		
    		transcoder.setSdReEncoderData(setSdReEncoderData(enabled));
    		transcoder.setVirtualChannel(virtualChannel);
    		transcoder.setTranscoderHDStatMux(setTranscoderHDStatMux(statMuxGroupIncluded));
    		transcoder.setUplinkBncInputControl(true);

    		transcoders.add(transcoder);
    	}
    	else {
        
    		transcoder = currentTranscoders.get(i);
    		
    		transcoder.setHdReEncoderData(setHdReEncoderData(enabled));
    		transcoder.setSdReEncoderData(setSdReEncoderData(enabled));
    		transcoder.setVirtualChannel(virtualChannel);
    		transcoder.setTranscoderHDStatMux(setTranscoderHDStatMux(statMuxGroupIncluded));
    		transcoder.setUplinkBncInputControl(true);

    		transcoders.add(transcoder);
    	}
    }
   
    //make sure we have setup the correct number of transcoders for this decoder
    int x = transcoders.size();
//    int defaultEnabled = Integer.parseInt(isAuthorizedExtTiers[0]);
//    boolean defaultStatMuxGroupIncluded = false;
//    if (hdStatMuxGroupEnabled == 1) {
//		defaultStatMuxGroupIncluded = defaultEnabled == 1;
//	}
	
    while (x++ < numTranscoders) {
    	
    	Transcoder blankTranscoder = TranscoderDto.getBlank();
    	
		blankTranscoder.setTranscoderIndex(x);
		blankTranscoder.setName(name + "-" + Integer.toString(x));
		
		Transcoder firstTranscoder = transcoders.get(0);
		blankTranscoder.setHdReEncoderData(setHdReEncoderData(0));//firstTranscoder.getHdReEncoderData());
		blankTranscoder.setSdReEncoderData(setSdReEncoderData(0));//firstTranscoder.getSdReEncoderData());
		blankTranscoder.setVirtualChannel(firstTranscoder.getVirtualChannel());
		blankTranscoder.setUplinkBncInputControl(false);//firstTranscoder.isUplinkBncInputControl());
			
		transcoders.add(blankTranscoder);
    }
    
    return transcoders;
  }

  private static HdReEncoderData setHdReEncoderData(int enabled) {
    logger.info("setHdReEncoderData - Entering");
    HdReEncoderData hdReEncoderData = new HdReEncoderData();

    logger.info("setHdReEncoderData - enabled: " + Integer.toString(enabled));

    hdReEncoderData.hdEnabled = (byte)enabled;

    hdReEncoderData.hdMinimumOutputVideoRateKbps = 15000;

    hdReEncoderData.hdOutputVideoResolutionFrameRate = HdOutputVideoResolutionFrameRateEnumType.V1080_I_AUTO;
    logger.info("setHdReEncoderData - Leaving");
    return hdReEncoderData;
  }

  private static SdReEncoderData setSdReEncoderData(int enabled) {
    logger.info("setSdReEncoderData - Entering");

    SdReEncoderData defaultSdReEncoderData = new SdReEncoderData();
    logger.info("setSdReEncoderData - enabled: " + Integer.toString(enabled));

    defaultSdReEncoderData.sdEnabled = (byte)enabled;

    defaultSdReEncoderData.mpeg2SdOutputVideoRateKbps = 5000;

    defaultSdReEncoderData.mpeg2SdOutputVideoResolution = Mpeg2SdOutputVideoResolutionEnumType.NTSC_528x480;

    defaultSdReEncoderData.defaultSdAspectRatio = DefaultSdAspectRatioEnumType.LETTER_BOX;

//    defaultSdReEncoderData.autoHdSdAspectRatioConversionMethod = AutoHdSdAspectRatioConversionMethodEnumType.AUTO_AFD;

    logger.info("setSdReEncoderData - Leaving");
    return defaultSdReEncoderData;
  }

  private static TranscoderHDStatMux setTranscoderHDStatMux(boolean statMuxGroupIncluded)
  {
    logger.info("setTranscoderHDStatMux - Entering");

    TranscoderHDStatMux transcoderHDStatMux = TranscoderHDStatMux.getBlank();

    transcoderHDStatMux.setStatMuxGroupIncluded(statMuxGroupIncluded);
    if (!statMuxGroupIncluded) {
      return transcoderHDStatMux;
    }

    transcoderHDStatMux.setStatMuxMaxBitRateKbps(18000);

    transcoderHDStatMux.setStatMuxMinBitRateKbps(5000);

    transcoderHDStatMux.setStatMuxWeight(5);
    logger.info("setTranscoderHDStatMux - Leaving");
    return transcoderHDStatMux;
  }
  
  //NEEDED FOR CONVERSION TO BNC 3.8
  private static SDIOutputControlType mappEmmProviderIdToSdiOutput(Integer emmProviderId) {
		
	  SDIOutputControlType sdiOutputControlType = SDIOutputControlType.IRD_USER_CONTROLLED;
	  if (emmProviderId != null) {
	  
		  //mapping from db TB_EMMProvider
		  switch (emmProviderId) {        
				case 0: { sdiOutputControlType = SDIOutputControlType.IRD_USER_CONTROLLED; break;}
				case 1: { sdiOutputControlType = SDIOutputControlType.SD_ONLY; break;}
				case 2: { sdiOutputControlType = SDIOutputControlType.NATIVE; break;}
		  }        
	  }	  
	  return sdiOutputControlType;
  }
}