package com.hbolag.bncauthcenter.utilities;

import java.sql.Timestamp;
import java.util.Calendar;

public class DateTimeHelper {
	
	public static Timestamp getTodayTimestamp ()
	{
		return new Timestamp(Calendar.getInstance().getTimeInMillis());
	}
}
