package com.hbolag.bncauthcenter.utilities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.motorola.bnc.common.infrastruct.errdiag.BNCEjbStatusException;
import com.motorola.bnc.common.shared.structures.Affiliate;
import com.motorola.bnc.common.shared.structures.AffiliateGroup;
import com.motorola.bnc.common.shared.structures.Channel;
import com.motorola.bnc.common.shared.structures.DecoderModel;
import com.motorola.bnc.common.shared.structures.DecoderModelType;
import com.motorola.bnc.common.shared.structures.HdSdReEncoderData;
import com.motorola.bnc.common.shared.structures.Location;
import com.motorola.bnc.common.shared.structures.MAD;
import com.motorola.bnc.common.shared.structures.RatingRegion;
import com.motorola.bnc.common.shared.structures.SatTrans;
import com.motorola.bnc.proxy.external.IExternalClientAPI;
import com.motorola.refarch.infrastruct.errdiag.MotStatusException;
import com.motorola.refarch.infrastruct.gendata.BaseOperatorGroup;
import com.motorola.refarch.infrastruct.gendata.BasePartition;
import com.motorola.refarch.infrastruct.gendata.BaseRegion;
import com.motorola.refarch.shared.auth.AuthorizationConceptsStatusException;
import com.motorola.refarch.shared.auth.DCIIUnitFilter;

public class BNCExternalClientAPIHelper
{
  private static final Logger logger = LogManager.getLogger(BNCExternalClientAPIHelper.class);

  public static Affiliate storeAffiliate(IExternalClientAPI bncExternalClientAPI, Affiliate arg0)
    throws MotStatusException
  {
    return bncExternalClientAPI.storeAffiliate(arg0);
  }

  public static AffiliateGroup storeAffiliateGroup(IExternalClientAPI bncExternalClientAPI, AffiliateGroup arg0)
    throws MotStatusException
  {
    return bncExternalClientAPI.storeAffiliateGroup(arg0);
  }

  public static Location storeLocation(IExternalClientAPI bncExternalClientAPI, Location arg0)
    throws MotStatusException
  {
    return bncExternalClientAPI.storeLocation(arg0);
  }

  public static AffiliateGroup queryAffiliateGroup(IExternalClientAPI bncExternalClientAPI, String affiliateGroup)
    throws MotStatusException
  {
    if (bncExternalClientAPI.affiliateGroupExists(affiliateGroup)) {
      return bncExternalClientAPI.queryAffiliateGroup(affiliateGroup);
    }
    return null;
  }

  public static Affiliate queryAffiliate(IExternalClientAPI bncExternalClientAPI, String affiliateName)
    throws MotStatusException
  {
    if (bncExternalClientAPI.affiliateExists(affiliateName)) {
      return bncExternalClientAPI.queryAffiliate(affiliateName);
    }
    return null;
  }

  public static Location queryLocation(IExternalClientAPI bncExternalClientAPI, String locationName, String affiliate)
    throws MotStatusException
  {
    if (bncExternalClientAPI.locationExists(locationName, affiliate)) {
      return bncExternalClientAPI.queryLocation(locationName, affiliate);
    }
    return null;
  }

  public static DecoderModel queryModel(IExternalClientAPI bncExternalClientAPI, DCIIUnitFilter arg0)
    throws BNCEjbStatusException
  {
    return bncExternalClientAPI.queryModel(arg0);
  }

  public static Collection<Affiliate> queryAllAffiliates(IExternalClientAPI bncExternalClientAPI)
    throws MotStatusException
  {
    return bncExternalClientAPI.queryAllAffiliates();
  }

  public static Collection<Location> queryAllLocations(IExternalClientAPI bncExternalClientAPI, String affiliate)
    throws MotStatusException
  {
    return bncExternalClientAPI.queryAllLocations(affiliate);
  }

  public static Collection<BaseOperatorGroup> queryAllOperatorGroups(IExternalClientAPI bncExternalClientAPI)
    throws MotStatusException
  {
    return bncExternalClientAPI.queryAllOperatorGroups();
  }

  public static Collection<BaseRegion> queryAllRegions(IExternalClientAPI bncExternalClientAPI, int operatorGroupUID)
    throws MotStatusException
  {
    return bncExternalClientAPI.queryAllRegions(operatorGroupUID);
  }

  public static Collection<RatingRegion> queryAllRatingRegions(IExternalClientAPI bncExternalClientAPI)
    throws MotStatusException
  {
    return bncExternalClientAPI.queryAllRatingRegions();
  }

  public static Collection<BasePartition> queryAllPartitions(IExternalClientAPI bncExternalClientAPI)
    throws MotStatusException
  {
    return bncExternalClientAPI.queryAllPartitions();
  }

  public static Collection<SatTrans> queryAllSatelliteTransponders(IExternalClientAPI bncExternalClientAPI)
    throws MotStatusException
  {
    return bncExternalClientAPI.queryAllSatelliteTransponders();
  }

  public static Collection<Channel> queryAllVirtualChannels(IExternalClientAPI bncExternalClientAPI)
    throws MotStatusException
  {
    return bncExternalClientAPI.queryAllVirtualChannels();
  }

  public static Collection<DCIIUnitFilter> queryAllIRDUnitAddresses(IExternalClientAPI bncExternalClientAPI, String locationName, String affiliateName)
    throws MotStatusException
  {
    return bncExternalClientAPI.queryAllIRDUnitAddresses(locationName, affiliateName);
  }

  public static Collection<DCIIUnitFilter> queryAllMADUnitAddresses(IExternalClientAPI bncExternalClientAPI, String locationName, String affiliateName) throws MotStatusException
  {
    return bncExternalClientAPI.queryAllMADUnitAddresses(locationName, affiliateName, DecoderModelType.MD);
  }

  public static Collection<DCIIUnitFilter> queryAllACPUnitAddresses(IExternalClientAPI bncExternalClientAPI, String anchorUnitAddresses) throws MotStatusException
  {
    return bncExternalClientAPI.queryAllACPUnitAddresses(new DCIIUnitFilter(anchorUnitAddresses));
  }

  public static Collection<DecoderModel> queryDecoderModels(IExternalClientAPI bncExternalClientAPI)
    throws MotStatusException
  {
    return bncExternalClientAPI.queryDecoderModels();
  }

  public static BasePartition getDefaultPartition(IExternalClientAPI bncExternalClientAPI)
    throws MotStatusException
  {
    Collection <BasePartition> partitions = queryAllPartitions(bncExternalClientAPI);
    if ((partitions == null) || (partitions.isEmpty())) {
      return null;
    }
    return (BasePartition)partitions.iterator().next();
  }

  public static Affiliate getDefaultAffiliate(IExternalClientAPI bncExternalClientAPI, BasePartition defaultBasePartition)
    throws MotStatusException
  {
    Collection<Affiliate> affiliates = queryAllAffiliates(bncExternalClientAPI);
    if ((affiliates == null) || (affiliates.isEmpty())) {
      return BNCExternalClientAPIUtility.setDefaultAffiliate(bncExternalClientAPI, defaultBasePartition);
    }
    return (Affiliate)affiliates.iterator().next();
  }

  public static Location getDefaultLocation(IExternalClientAPI bncExternalClientAPI, Affiliate defaultAffiliate, BasePartition defaultBasePartition, RatingRegion defaultRatingRegion) throws MotStatusException
  {
    Collection<Location> locations = queryAllLocations(bncExternalClientAPI, defaultAffiliate.getName());
    if ((locations == null) || (locations.isEmpty())) {
      return BNCExternalClientAPIUtility.setDefaultLocation(bncExternalClientAPI, defaultBasePartition, defaultRatingRegion, defaultAffiliate);
    }
    return (Location)locations.iterator().next();
  }

  public static BaseOperatorGroup getDefaultOperatorGroup(IExternalClientAPI bncExternalClientAPI) throws MotStatusException
  {
    Collection<BaseOperatorGroup> operatorGroups = queryAllOperatorGroups(bncExternalClientAPI);
    if ((operatorGroups == null) || (operatorGroups.isEmpty())) {
      return null;
    }
    return (BaseOperatorGroup)operatorGroups.iterator().next();
  }

  public static BaseRegion getDefaultRegion(IExternalClientAPI bncExternalClientAPI, int operatorGroupUID) throws MotStatusException
  {
    Collection<BaseRegion> regions = queryAllRegions(bncExternalClientAPI, operatorGroupUID);
    if ((regions == null) || (regions.isEmpty())) {
      return null;
    }
    return (BaseRegion)regions.iterator().next();
  }

  public static RatingRegion getDefaultRatingRegion(IExternalClientAPI bncExternalClientAPI) throws MotStatusException
  {
    Collection<RatingRegion> ratingRegions = queryAllRatingRegions(bncExternalClientAPI);
    if ((ratingRegions == null) || (ratingRegions.isEmpty())) {
      return null;
    }
    return (RatingRegion)ratingRegions.iterator().next();
  }

  public static DecoderModel getDefaultDecoderModel(IExternalClientAPI bncExternalClientAPI, String decoderModel, String decoderModelType) throws MotStatusException
  {
    Collection <DecoderModel>decoderModels = queryDecoderModels(bncExternalClientAPI);
    if ((decoderModels == null) || (decoderModels.isEmpty())) {
      return getGenericModel(decoderModelType);
    }
    for (DecoderModel decoderModel2 : decoderModels)
    {
      if (decoderModel2.getKey().matches(decoderModel))
      {
        return decoderModel2;
      }
    }
    return getGenericModel(decoderModelType);
  }

  public static MAD getMADUnitAddress(IExternalClientAPI bncExternalClientAPI, String anchorUnitAddress) throws MotStatusException {
    DCIIUnitFilter unitFilter = new DCIIUnitFilter(anchorUnitAddress);

    if (bncExternalClientAPI.madExists(unitFilter)) {
      return bncExternalClientAPI.queryMAD(unitFilter);
    }
    return null;
  }

  public static boolean isMADEmpty(IExternalClientAPI bncExternalClientAPI, String anchorUnitAddress) throws AuthorizationConceptsStatusException, MotStatusException
  {
    Collection<DCIIUnitFilter> ACPUnitAddresses = bncExternalClientAPI.queryAllACPUnitAddresses(new DCIIUnitFilter(anchorUnitAddress));

    return (ACPUnitAddresses == null) || (ACPUnitAddresses.isEmpty());
  }

  public static MAD storeMADUnitAddress(IExternalClientAPI bncExternalClientAPI, MAD newMad)
    throws MotStatusException
  {
    return bncExternalClientAPI.storeMAD(newMad, true);
  }

  public static HdSdReEncoderData queryHdSdReEncoderDefaults(IExternalClientAPI bncExternalClientAPI) throws MotStatusException {
    return bncExternalClientAPI.queryHdSdReEncoderDefaults();
  }

  private static DecoderModel getGenericModel(String decoderModelType) {
    DecoderModelType modelType = null;

    if (decoderModelType == DecoderModelType.IRD.getName())
      modelType = DecoderModelType.IRD;
    else if (decoderModelType == DecoderModelType.MD.getName()) {
      modelType = DecoderModelType.MD;
    }
    return DecoderModel.getGenericModel(modelType);
  }

  public static Channel findVirtualChannel(int vctID, int tierNumber, IExternalClientAPI bncExternalClientAPI) throws MotStatusException {
    int virtualNumber = Integer.parseInt(Integer.toString(tierNumber) + "0");
    Collection<Channel> channels = bncExternalClientAPI.queryAllVirtualChannels();
    for (Channel channel : channels) {
      if ((channel.getVCMID() == vctID) && (channel.getNumber() == virtualNumber))
        return channel;
    }
    return null;
  }

  public static List<Channel> setVirtualChannelsDSR_640N(int vctID, String[] tierNumbers, String[] IsAuthorizedExtTiers, IExternalClientAPI bncExternalClientAPI) throws MotStatusException {
	  List<Channel> virtualChannels = new ArrayList<Channel>();
	  Collection<Channel> channels = bncExternalClientAPI.queryAllVirtualChannels();
	  Channel lastEnabledChannel = Channel.getBlank();

	  logger.info("setVirtualChannels - VCMID " + Integer.toString(vctID));

	  for (int i = 0; i < tierNumbers.length; i++) {
    	
		  int virtualNumber = Integer.parseInt(tierNumbers[i] + "0");
		  if (tierNumbers[i].equals("50")) {
			  virtualNumber = 510;
		  }
		  else if (tierNumbers[i].equals("140")) {
			  virtualNumber = 1410;
		  }

		  logger.info("setVirtualChannels - VirtualNumber " + Integer.toString(virtualNumber));

		  boolean isFound = false;
		  for (Channel channel : channels) {
			  if ((channel.getVCMID() != vctID) || (channel.getNumber() != virtualNumber)) 
				  continue;
			  logger.info("setVirtualChannels - Channel found for VCMID");
			  virtualChannels.add(channel);
			  lastEnabledChannel = channel;
			  isFound = true;
			  break;
		  }

		  if (isFound)
			  continue;
		  logger.info("setVirtualChannels - not found");
		  virtualChannels.add(lastEnabledChannel);
	  }

	  return virtualChannels;
  }
}

/* Location:           C:\dev\BNCCenter\BNCAUCenterWS_Prod\BNCAUCenterWS\WEB-INF\classes\
 * Qualified Name:     com.hbolag.bncauthcenter.utilities.BNCExternalClientAPIHelper
 * JD-Core Version:    0.6.0
 */