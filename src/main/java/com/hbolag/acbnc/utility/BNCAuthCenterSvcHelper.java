package com.hbolag.acbnc.utility;

import com.hbolag.bncauthcenter.structures.UnitData;

public class BNCAuthCenterSvcHelper {
	
	public static UnitData setUnitData(int encoderGroupID, String anchorUnitAddress, String unitAddress, int tierNumber, String decoderModelType, String decoderModel, int templateID, int singleVCMID, String cableCode, int headEndCode, int countryID, String serialNumber, boolean isAuthorized)
	{
		UnitData newUnitData = new UnitData();
		
		newUnitData.setEncoderGroupID(encoderGroupID);
		newUnitData.setAnchorUnitAddress(anchorUnitAddress);
		newUnitData.setUnitAddress(unitAddress);
		newUnitData.setTierNumber(tierNumber);
		newUnitData.setDecoderModelType(decoderModelType);
		newUnitData.setDecoderModel(decoderModel);
		newUnitData.setTemplateID(templateID);
		newUnitData.setSingleVCMID(singleVCMID);
		newUnitData.setCableCode(cableCode);
		newUnitData.setHeadEndCode(headEndCode);
		newUnitData.setCountryID(countryID);
		newUnitData.setSerialNumber(serialNumber);
		newUnitData.setIsAuthorized(isAuthorized);
		
		return newUnitData;
	}
	
	public static UnitData setUnitData(int encoderGroupID, String anchorUnitAddress, String unitAddress, int tierNumber, String tierNumberExt, String decoderModelType, String decoderModel, int templateID, int singleVCMID, String cableCode, int headEndCode, int countryID, String serialNumber, boolean isAuthorized, String isAuthorizedExt, boolean templateTierExtended, Integer transportStreamOutput, Boolean frontPanelEnabled, Boolean eMMStream, Integer eMMProviderId)
	{
		UnitData newUnitData = new UnitData();
		
		newUnitData.setEncoderGroupID(encoderGroupID);
		newUnitData.setAnchorUnitAddress(anchorUnitAddress);
		newUnitData.setUnitAddress(unitAddress);
		newUnitData.setTierNumber(tierNumber);
		newUnitData.setTierNumberExt(tierNumberExt);
		newUnitData.setDecoderModelType(decoderModelType);
		newUnitData.setDecoderModel(decoderModel);
		newUnitData.setTemplateID(templateID);
		newUnitData.setSingleVCMID(singleVCMID);
		newUnitData.setCableCode(cableCode);
		newUnitData.setHeadEndCode(headEndCode);
		newUnitData.setCountryID(countryID);
		newUnitData.setSerialNumber(serialNumber);
		newUnitData.setIsAuthorized(isAuthorized);
	    newUnitData.setIsAuthorizedExt(isAuthorizedExt);
	    newUnitData.setTemplateTierExtended(templateTierExtended);
		newUnitData.setTransportStreamOutput(transportStreamOutput);
		newUnitData.setFrontPanelEnabled(frontPanelEnabled);
	    newUnitData.setEMMStream(eMMStream);
	    newUnitData.setEMMProviderId(eMMProviderId);	
		return newUnitData;
	}
}