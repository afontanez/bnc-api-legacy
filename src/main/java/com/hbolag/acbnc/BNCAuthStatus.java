package com.hbolag.acbnc;

public class BNCAuthStatus {	
	//Constructor
	public BNCAuthStatus(){
		SourceModuleCode = 0;
		StatusClass		 = 0;
		StatusCode		 = 0;
	}	
	public int SourceModuleCode;
	public int StatusClass;
	public int StatusCode;
}