package com.hbolag.acbnc;

import java.net.UnknownHostException;
import java.util.ResourceBundle;

import javax.jws.WebService;
import javax.naming.NamingException;
import javax.security.auth.login.LoginException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hbolag.acbnc.utility.BNCAuthCenterSvcHelper;
import com.hbolag.bncauthcenter.BNCAuthCenterAPIFactory;
import com.hbolag.bncauthcenter.IBNCAuthCenterAPI;
import com.hbolag.bncauthcenter.structures.UnitData;
import com.motorola.refarch.infrastruct.errdiag.MotStatusException;


@WebService(targetNamespace = "http://acbnc.hbolag.com/", endpointInterface = "com.hbolag.acbnc.IBNCAuthCenterSvc", portName = "BNCAuthCenterSvcPort", serviceName = "BNCAuthCenterSvcService")
public class BNCAuthCenterSvc implements IBNCAuthCenterSvc {
	
	private static final Logger logger = LogManager.getLogger(BNCAuthCenterSvc.class);
		
	@Override
	public String getVersion(){
		
//		String username = "motfield";
//		String password = "bnc2evnbtr!";
		//String serverIPAddres = "172.24.100.5"; //PROD
//		String serverIPAddres = "121.120.1.211"; //TEST
		IBNCAuthCenterAPI bncAuthCenterAPI = null;
		String version = null;
		
		try {
	
			//Get reference to SQL Database configuration
	    	ResourceBundle rb = ResourceBundle.getBundle("connection_config");
	    	
	    	//Initialize sql variables
	    	String username = rb.getString("bnc.user");
	    	String password = rb.getString("bnc.password");
	    	String serverIPAddress = rb.getString("bnc.ip");
	    	
			logger.info("getVersion");
			bncAuthCenterAPI = BNCAuthCenterAPIFactory.getBNCAuthCenterAPI(username, password, serverIPAddress);
		} catch (UnknownHostException e1) {
			logger.error("getVersion: " + e1.getMessage());
			e1.printStackTrace();
		} catch (LoginException e1) {
			logger.error("getVersion: " + e1.getMessage());
			e1.printStackTrace();
		} catch (NamingException e1) {
			logger.error("getVersion: " + e1.getMessage());
			e1.printStackTrace();
		} catch (MotStatusException e1) {
			logger.error("getVersion: " + e1.getMessage());
			e1.printStackTrace();
		}

		if (bncAuthCenterAPI != null)
		{
			//Get BNC Version
		 	try {
		 		logger.info("BNC Version: " + bncAuthCenterAPI.getBNCVersion());
		 		version = bncAuthCenterAPI.getBNCVersion();
			} catch (MotStatusException e) {
				logger.error("getVersion: " + e.getMessage());
				e.printStackTrace();
			}
		 	finally {
		 		//Disconnect
			 	BNCAuthCenterAPIFactory.releaseBNCAuthCenterAPI(bncAuthCenterAPI);
			}
		}
		
		return version;
	}
	
	@Override
	public BNCAuthStatus setIRD(
			String userName,
			String password,
			String serverIPAddres,
			int encoderGroupID,
			String anchorUnitAddress,
			String unitAddress,
			int tierNumber,
			String tierNumberExt,
			String decoderModel,
			int templateID,
			int singleVCMID,
			String cableCode,
			int headEndCode,
			int countryID,
			String serialNumber,
			boolean isAuthorized,
			String isAuthorizedExt,
			boolean templateTierExtended,
			Integer transportStreamOutput,
			Boolean frontPanelEnabled,
			Boolean eMMStream,
		    Integer eMMProviderId) {
		
		IBNCAuthCenterAPI bncAuthCenterAPI 	= null;
		UnitData unitData 					= new UnitData();
		BNCAuthStatus bncAuthStatus 		= new BNCAuthStatus();
		

		try {
		      logger.info("BNCAuthCenterSvc.setIRD");
		      bncAuthCenterAPI = BNCAuthCenterAPIFactory.getBNCAuthCenterAPI(userName, password, serverIPAddres);
		      
		} catch (UnknownHostException uhe) {
		      logger.error("BNCAuthCenterSvc.setIRD - UnknownHostException: " + uhe.getMessage());
		} catch (LoginException le) {
		      logger.error("BNCAuthCenterSvc.setIRD - LoginException: " + le.getMessage());
		} catch (NamingException ne) {
		      logger.error("BNCAuthCenterSvc.setIRD -NamingException: " + ne.getMessage());
		} catch (MotStatusException mse) {
		      logger.error("BNCAuthCenterSvc.setIRD - BNCAuthCenterAPIFactory.getBNCAuthCenterAPI: " + mse.getMessage());
		      bncAuthStatus.SourceModuleCode = mse.getWhy().getSourceModuleCode();
		      bncAuthStatus.StatusClass = mse.getWhy().getStatusClass();
		      bncAuthStatus.StatusCode = mse.getWhy().getStatusCode();
		      return bncAuthStatus;
		}

		if (bncAuthCenterAPI != null)
		{
			//Set IRD
		 	try {
		 		//Set Unit Data
		 		unitData = BNCAuthCenterSvcHelper.setUnitData(encoderGroupID, anchorUnitAddress, unitAddress, tierNumber, tierNumberExt, "IRD", decoderModel, templateID, singleVCMID, cableCode, headEndCode, countryID, serialNumber, isAuthorized, isAuthorizedExt, templateTierExtended, transportStreamOutput, frontPanelEnabled, eMMStream, eMMProviderId);	
		 		//Set IRD
		 		bncAuthCenterAPI.setIRD(unitData);
			} catch (MotStatusException e) {
				logger.error("BNCAuthCenterSvc.setIRD - bncAuthCenterAPI.setIRD: " + e.getMessage());
			    bncAuthStatus.SourceModuleCode = e.getWhy().getSourceModuleCode();
			    bncAuthStatus.StatusClass = e.getWhy().getStatusClass();
			    bncAuthStatus.StatusCode = e.getWhy().getStatusCode();
			} 
		 	catch (Exception e) {
		 		logger.error("BNCAuthCenterSvc.setIRD - bncAuthCenterAPI.setIRD: " + e.getMessage());
			    bncAuthStatus.SourceModuleCode = -1;
			    bncAuthStatus.StatusClass = -1;
			    bncAuthStatus.StatusCode = -1;
			    
			    e.printStackTrace();
		 	}
		 	finally {
			 	//Disconnect
		 		try {
		 			BNCAuthCenterAPIFactory.releaseBNCAuthCenterAPI(bncAuthCenterAPI);
		 		}
		 		catch (Exception e) {
		 			e.printStackTrace();
		 		}
			}
		}
		return bncAuthStatus;
	}
	
	@Override
	public void tripIRD(String userName, String password, String serverIPAddres, String irdUnitAddress)  {
		
		IBNCAuthCenterAPI bncAuthCenterAPI = null;

		try {
			logger.info("BNCAuthCenterSvc.tripIRD");
			bncAuthCenterAPI = BNCAuthCenterAPIFactory.getBNCAuthCenterAPI(userName, password, serverIPAddres);
		} catch (UnknownHostException uhe) {
		      logger.error("BNCAuthCenterSvc.tripIRD - UnknownHostException: " + uhe.getMessage());
	    } catch (LoginException le) {
	      logger.error("BNCAuthCenterSvc.tripIRD - LoginException: " + le.getMessage());
	    } catch (NamingException ne) {
	      logger.error("BNCAuthCenterSvc.tripIRD - NamingException: " + ne.getMessage());
	    } catch (MotStatusException mse) {
	      logger.error("BNCAuthCenterSvc.tripIRD - BNCAuthCenterAPIFactory.getBNCAuthCenterAPI: " + mse.getMessage());
	    }

		if (bncAuthCenterAPI != null)
		{
			//Trip IRD
		 	try {
		 		if (!irdUnitAddress.isEmpty())
		 			bncAuthCenterAPI.tripIRD(irdUnitAddress);
				
			} catch (MotStatusException e) {
		        logger.error("BNCAuthCenterSvc.tripIRD - bncAuthCenterAPI.tripIRD: " + e.getMessage());
			}
		 	finally {
		 		//Disconnect
			 	BNCAuthCenterAPIFactory.releaseBNCAuthCenterAPI(bncAuthCenterAPI);
			}
		}
	}
	
	@Override
	public BNCAuthStatus deleteIRD(
			String userName,
			String password,
			String serverIPAddres,
			String irdUnitAddress) {
		IBNCAuthCenterAPI bncAuthCenterAPI 	= null;
		BNCAuthStatus bncAuthStatus 		= new BNCAuthStatus();

		try {
			  logger.info("BNCAuthCenterSvc.deleteIRD");
			  bncAuthCenterAPI = BNCAuthCenterAPIFactory.getBNCAuthCenterAPI(userName, password, serverIPAddres);
		} catch (UnknownHostException uhe) {
		      logger.error("BNCAuthCenterSvc.deleteIRD - UnknownHostException: " + uhe.getMessage());
	    } catch (LoginException le) {
	          logger.error("BNCAuthCenterSvc.deleteIRD - LoginException: " + le.getMessage());
	    } catch (NamingException ne) {
	           logger.error("BNCAuthCenterSvc.deleteIRD - NamingException: " + ne.getMessage());
	    } catch (MotStatusException mse) {
		       logger.error("BNCAuthCenterSvc.deleteIRD - BNCAuthCenterAPIFactory.getBNCAuthCenterAPI: " + mse.getMessage());
		       bncAuthStatus.SourceModuleCode = mse.getWhy().getSourceModuleCode();
		       bncAuthStatus.StatusClass = mse.getWhy().getStatusClass();
		       bncAuthStatus.StatusCode = mse.getWhy().getStatusCode();
	      return bncAuthStatus;
	    }

		if (bncAuthCenterAPI != null)
		{
			//Delete IRD
		 	try {
		 		if (!irdUnitAddress.isEmpty())
		 			bncAuthCenterAPI.deleteIRD(irdUnitAddress);
				
			} catch (MotStatusException e) {
		        logger.error("BNCAuthCenterSvc.deleteIRD - bncAuthCenterAPI.deleteIRD: " + e.getMessage());
		        bncAuthStatus.SourceModuleCode = e.getWhy().getSourceModuleCode();
		        bncAuthStatus.StatusClass = e.getWhy().getStatusClass();
		        bncAuthStatus.StatusCode = e.getWhy().getStatusCode();
			}
		 	finally {
		 		//Disconnect
			 	BNCAuthCenterAPIFactory.releaseBNCAuthCenterAPI(bncAuthCenterAPI);
			}
		}
		return bncAuthStatus;
	}
	
	@Override
	public BNCAuthStatus setACP(
			String userName,
			String password,
			String serverIPAddres,
			int encoderGroupID,
			String anchorUnitAddress,
			String acpUnitAddress,
			int tierNumber,
			String decoderModel,
			int singleVCMID,
			String cableCode,
			int headEndCode,
			int countryID,
			String serialNumber,
			boolean isAuthorized) {
		
		UnitData unitData 					= new UnitData();
		IBNCAuthCenterAPI bncAuthCenterAPI 	= null;
		BNCAuthStatus bncAuthStatus 		= new BNCAuthStatus();

	    try
	    {
	      logger.info("BNCAuthCenterSvc.setACP");
	      bncAuthCenterAPI = BNCAuthCenterAPIFactory.getBNCAuthCenterAPI(userName, password, serverIPAddres);
	    } catch (UnknownHostException uhe) {
	      logger.error("BNCAuthCenterSvc.setACP - UnknownHostException: " + uhe.getMessage());
	    } catch (LoginException le) {
	      logger.error("BNCAuthCenterSvc.setACP - LoginException: " + le.getMessage());
	    } catch (NamingException ne) {
	      logger.error("BNCAuthCenterSvc.setACP - NamingException: " + ne.getMessage());
	    } catch (MotStatusException mse) {
	      logger.error("BNCAuthCenterSvc.setACP - BNCAuthCenterAPIFactory.getBNCAuthCenterAPI: " + mse.getMessage());
	      bncAuthStatus.SourceModuleCode = mse.getWhy().getSourceModuleCode();
	      bncAuthStatus.StatusClass = mse.getWhy().getStatusClass();
	      bncAuthStatus.StatusCode = mse.getWhy().getStatusCode();
	      return bncAuthStatus;
	    }

	    if (bncAuthCenterAPI != null)
	    {
	      try
	      {
	        unitData = BNCAuthCenterSvcHelper.setUnitData(encoderGroupID, anchorUnitAddress, acpUnitAddress, tierNumber, "MD", decoderModel, 0, singleVCMID, cableCode, headEndCode, countryID, serialNumber, isAuthorized);
	        logger.info("BNCAuthCenterSvc.setACP - Params: " + unitData.toString());
	        bncAuthCenterAPI.setACP(unitData);
	      } catch (MotStatusException e) {
	        logger.error("BNCAuthCenterSvc.setACP - BNCAuthCenterSvcHelper.setUnitData: " + e.getMessage());
	        bncAuthStatus.SourceModuleCode = e.getWhy().getSourceModuleCode();
	        bncAuthStatus.StatusClass = e.getWhy().getStatusClass();
	        bncAuthStatus.StatusCode = e.getWhy().getStatusCode();
	      }
	      finally
	      {
	        BNCAuthCenterAPIFactory.releaseBNCAuthCenterAPI(bncAuthCenterAPI);
	      }
	    }
	    return bncAuthStatus;
	}
	
	@Override
	public void tripACP(String userName, String password,
			String serverIPAddres, String acpUnitAddress) {
		
		IBNCAuthCenterAPI bncAuthCenterAPI = null;

	    try
	    {
	      logger.info("BNCAuthCenterSvc.tripACP");
	      bncAuthCenterAPI = BNCAuthCenterAPIFactory.getBNCAuthCenterAPI(userName, password, serverIPAddres);
	    } catch (UnknownHostException uhe) {
	      logger.error("BNCAuthCenterSvc.tripACP - UnknownHostException: " + uhe.getMessage());
	    } catch (LoginException le) {
	      logger.error("BNCAuthCenterSvc.tripACP - LoginException: " + le.getMessage());
	    } catch (NamingException ne) {
	      logger.error("BNCAuthCenterSvc.tripACP - NamingException: " + ne.getMessage());
	    } catch (MotStatusException mse) {
	      logger.error("BNCAuthCenterSvc.tripACP - BNCAuthCenterAPIFactory.getBNCAuthCenterAPI: " + mse.getMessage());
	    }

		if (bncAuthCenterAPI != null)
		{
			//Trip ACP
		 	try {
		 		if (!acpUnitAddress.isEmpty())
		 			bncAuthCenterAPI.tripACP(acpUnitAddress);
				
			} catch (MotStatusException e) {
				logger.error("tripACP - bncAuthCenterAPI.tripACP: " + e.getMessage());
			}
		 	finally {
		 		//Disconnect
		 		 BNCAuthCenterAPIFactory.releaseBNCAuthCenterAPI(bncAuthCenterAPI);
			}
		}
	}
	
	@Override
	public BNCAuthStatus deleteACP(String userName, String password,
			String serverIPAddres, String anchorUnitAddress, String acpUnitAddress) {
		   IBNCAuthCenterAPI bncAuthCenterAPI = null;
		    BNCAuthStatus bncAuthStatus = new BNCAuthStatus();
		    try
		    {
		      logger.info("BNCAuthCenterSvc.deleteACP");
		      bncAuthCenterAPI = BNCAuthCenterAPIFactory.getBNCAuthCenterAPI(userName, password, serverIPAddres);
		    } catch (UnknownHostException uhe) {
		      logger.error("BNCAuthCenterSvc.deleteACP - UnknownHostException: " + uhe.getMessage());
		    } catch (LoginException le) {
		      logger.error("BNCAuthCenterSvc.deleteACP - LoginException: " + le.getMessage());
		    } catch (NamingException ne) {
		      logger.error("BNCAuthCenterSvc.deleteACP - NamingException: " + ne.getMessage());
		    } catch (MotStatusException mse) {
		      logger.error("BNCAuthCenterSvc.deleteACP - BNCAuthCenterAPIFactory.getBNCAuthCenterAPI: " + mse.getMessage());
		      bncAuthStatus.SourceModuleCode = mse.getWhy().getSourceModuleCode();
		      bncAuthStatus.StatusClass = mse.getWhy().getStatusClass();
		      bncAuthStatus.StatusCode = mse.getWhy().getStatusCode();
		      return bncAuthStatus;
		    }

		    if (bncAuthCenterAPI != null)
		    {
		      try
		      {
		        if (!acpUnitAddress.isEmpty())
		          bncAuthCenterAPI.deleteACP(anchorUnitAddress, acpUnitAddress);
		      } catch (MotStatusException e) {
		        logger.error("BNCAuthCenterSvc.deleteACP - bncAuthCenterAPI.deleteACP: " + e.getMessage());
		        bncAuthStatus.SourceModuleCode = e.getWhy().getSourceModuleCode();
		        bncAuthStatus.StatusClass = e.getWhy().getStatusClass();
		        bncAuthStatus.StatusCode = e.getWhy().getStatusCode();
		      }
		      finally
		      {
		        BNCAuthCenterAPIFactory.releaseBNCAuthCenterAPI(bncAuthCenterAPI);
		      }
		    }
		    return bncAuthStatus;
	}
	
	@Override
	public void queryAllIRDUnitAddresses(String userName, String password,
			String serverIPAddres) {
		
	    IBNCAuthCenterAPI bncAuthCenterAPI = null;
	    try {
	      logger.info("BNCAuthCenterSvc.queryAllIRDUnitAddresses");
	      bncAuthCenterAPI = BNCAuthCenterAPIFactory.getBNCAuthCenterAPI(userName, password, serverIPAddres);
	    } catch (UnknownHostException uhe) {
	      logger.error("BNCAuthCenterSvc.queryAllIRDUnitAddresses - UnknownHostException: " + uhe.getMessage());
	    } catch (LoginException le) {
	      logger.error("BNCAuthCenterSvc.queryAllIRDUnitAddresses - LoginException: " + le.getMessage());
	    } catch (NamingException ne) {
	      logger.error("BNCAuthCenterSvc.queryAllIRDUnitAddresses - NamingException: " + ne.getMessage());
	    } catch (MotStatusException mse) {
	      logger.error("BNCAuthCenterSvc.queryAllIRDUnitAddresses - BNCAuthCenterAPIFactory.getBNCAuthCenterAPI: " + mse.getMessage());
	    }

	    if (bncAuthCenterAPI != null)
	    {
	      try
	      {
	        bncAuthCenterAPI.queryAllIRDUnitAddresses(serverIPAddres);
	      } catch (MotStatusException e) {
	        logger.error("BNCAuthCenterSvc.queryAllIRDUnitAddresses - bncAuthCenterAPI.queryAllIRDUnitAddresses: " + e.getMessage());
	      }
	      finally
	      {
	        BNCAuthCenterAPIFactory.releaseBNCAuthCenterAPI(bncAuthCenterAPI);
	      }
	    }

	}
	
	@Override
	public void queryAllACPUnitAddresses(String userName, String password,
			String serverIPAddres) {
		
	    IBNCAuthCenterAPI bncAuthCenterAPI = null;
	    try
	    {
	      logger.info("BNCAuthCenterSvc.queryAllACPUnitAddresses");
	      bncAuthCenterAPI = BNCAuthCenterAPIFactory.getBNCAuthCenterAPI(userName, password, serverIPAddres);
	    } catch (UnknownHostException uhe) {
	      logger.error("BNCAuthCenterSvc.queryAllACPUnitAddresses - UnknownHostException: " + uhe.getMessage());
	    } catch (LoginException le) {
	      logger.error("BNCAuthCenterSvc.queryAllACPUnitAddresses - LoginException: " + le.getMessage());
	    } catch (NamingException ne) {
	      logger.error("BNCAuthCenterSvc.queryAllACPUnitAddresses - NamingException: " + ne.getMessage());
	    } catch (MotStatusException mse) {
	      logger.error("BNCAuthCenterSvc.queryAllACPUnitAddresses BNCAuthCenterAPIFactory.getBNCAuthCenterAPI: " + mse.getMessage());
	    }

	    if (bncAuthCenterAPI != null)
	    {
	      try
	      {
	        bncAuthCenterAPI.queryAllMADUnitAddresses(serverIPAddres);
	      } catch (MotStatusException e) {
	        logger.error("BNCAuthCenterSvc.queryAllACPUnitAddresses - bncAuthCenterAPI.queryAllMADUnitAddresses: " + e.getMessage());
	      }
	      finally
	      {
	        BNCAuthCenterAPIFactory.releaseBNCAuthCenterAPI(bncAuthCenterAPI);
	      }
	    }
	  }
	
}
