package com.hbolag.acbnc;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(name = "IBNCAuthCenterSvc", targetNamespace = "http://acbnc.hbolag.com/")
public interface IBNCAuthCenterSvc {
	
	@WebMethod(operationName = "getVersion", action = "urn:GetVersion")
	String getVersion();
	@WebMethod(operationName = "setIRD", action = "urn:SetIRD")
	
	BNCAuthStatus setIRD(@WebParam(name="arg0") String userName, @WebParam(name="arg1") String password, @WebParam(name="arg2") String serverIPAddres, @WebParam(name="arg3") int encoderGroupID, @WebParam(name="arg4") String anchorUnitAddress, @WebParam(name="arg5") String unitAddress, @WebParam(name="arg6") int tierNumber, @WebParam(name="arg7") String tierNumberExt, @WebParam(name="arg8") String decoderModel, @WebParam(name="arg9") int templateID, @WebParam(name="arg10") int singleVCMID, @WebParam(name="arg11") String cableCode, @WebParam(name="arg12") int headEndCode, @WebParam(name="arg13") int countryID, @WebParam(name="arg14") String serialNumber, @WebParam(name="arg15") boolean isAuthorized, @WebParam(name="arg16") String isAuthorizedExt, @WebParam(name="arg17") boolean templateTierExtended, @WebParam(name="arg18") Integer transportStreamOutput, @WebParam(name="arg19") Boolean frontPanelEnabled, @WebParam(name="arg20") Boolean eMMStream, @WebParam(name="arg21") Integer eMMProviderId);
	@WebMethod(operationName = "tripIRD", action = "urn:TripIRD")
	void tripIRD(@WebParam(name = "arg0") String userName, @WebParam(name = "arg1") String password, @WebParam(name = "arg2") String serverIPAddres,@WebParam(name = "arg3") String irdUnitAddress);
	
	@WebMethod(operationName = "deleteIRD", action = "urn:DeleteIRD")
	BNCAuthStatus deleteIRD(@WebParam(name = "arg0") String userName, @WebParam(name = "arg1") String password, @WebParam(name = "arg2") String serverIPAddres, @WebParam(name = "arg3") String irdUnitAddress);
	@WebMethod(operationName = "queryAllACPUnitAddresses", action = "urn:QueryAllACPUnitAddresses")
	void queryAllACPUnitAddresses(@WebParam(name = "arg0") String userName, @WebParam(name = "arg1") String password, @WebParam(name = "arg2") String serverIPAddres);
	@WebMethod(operationName = "setACP", action = "urn:SetACP")
	BNCAuthStatus setACP(@WebParam(name = "arg0") String userName, @WebParam(name = "arg1") String password, @WebParam(name = "arg2") String serverIPAddres, @WebParam(name = "arg3") int encoderGroupID, @WebParam(name = "arg4") String anchorUnitAddress, @WebParam(name = "arg5") String acpUnitAddress, @WebParam(name = "arg6") int tierNumber, @WebParam(name = "arg7") String decoderModel, @WebParam(name = "arg8") int singleVCMID, @WebParam(name = "arg9") String cableCode, @WebParam(name = "arg10") int headEndCode,@WebParam(name = "arg11") int countryID, @WebParam(name = "arg12") String serialNumber, @WebParam(name = "arg13") boolean isAuthorized);
	
	@WebMethod(operationName = "tripACP", action = "urn:TripACP")
	void tripACP(@WebParam(name = "arg0") String userName, @WebParam(name = "arg1") String password, @WebParam(name = "arg2") String serverIPAddres, @WebParam(name = "arg3") String acpUnitAddress);
	@WebMethod(operationName = "deleteACP", action = "urn:DeleteACP")
	BNCAuthStatus deleteACP(@WebParam(name="arg0") String userName, @WebParam(name="arg1") String password, @WebParam(name="arg2") String serverIPAddres, @WebParam(name="arg3") String anchorUnitAddress, @WebParam(name="arg4") String acpUnitAddress);
	@WebMethod(operationName = "queryAllIRDUnitAddresses", action = "urn:QueryAllIRDUnitAddresses")
	void queryAllIRDUnitAddresses(@WebParam(name = "arg0") String userName, @WebParam(name = "arg1") String password, @WebParam(name = "arg2") String serverIPAddres);
}