package com.hbolag.bncauthcenter.utilities;

import java.util.List;

import org.junit.Test;
import static org.junit.Assert.*;

import com.motorola.refarch.infrastruct.gendata.BaseTier;

public class BNCAuthCenterHelperTest {

	@Test
	public void setTiers2() {
		
		List<BaseTier> list = BNCAuthCenterHelper.setTiers_640N(null, null);
		assertNotNull(list);
		assertTrue(list.isEmpty());
		
		list = BNCAuthCenterHelper.setTiers_640N(new String[1], null);
		assertNotNull(list);
		assertTrue(list.isEmpty());
		
		list = BNCAuthCenterHelper.setTiers_640N(null, new String[1]);
		assertNotNull(list);
		assertTrue(list.isEmpty());
		
		String[] tiers = {"!@#", "87", null};
		String[] isTierAuthorized = {"1","1", "1"};
		list = BNCAuthCenterHelper.setTiers_640N(tiers, isTierAuthorized);
		assertNotNull(list);
		assertEquals(list.size(), 1);
		assertEquals(list.get(0).getNumber(), 87);
		
		String[] tiers2 = {"09", "87", "12"};
		String[] isTierAuthorized2 = {"1","0", "1"};
		list = BNCAuthCenterHelper.setTiers_640N(tiers2, isTierAuthorized2);
		assertNotNull(list);
		assertEquals(list.size(), 2);
		assertEquals(list.get(0).getNumber(), 9);
		assertEquals(list.get(1).getNumber(), 12);
	}
	
	
	@Test
	public void getArrayOfTiers() {
		
		int[] arry = BNCAuthCenterHelper.getArrayOfTiers(159400400, 356, false);//boolean templateTierExtended) {
		
		for (int i : arry) {
		
			System.out.println(i);
		}
		
		arry = BNCAuthCenterHelper.getArrayOfTiers(159400400, 356, true);//boolean templateTierExtended) {
		
		for (int x : arry) {
			
			System.out.println(x);
		}
		
	}
	
}
